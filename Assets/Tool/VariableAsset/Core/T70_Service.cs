using System;
using System.Collections;
using UnityEngine;

namespace T70
{
    public class T70_Service : MonoBehaviour
    {
        public enum State
        {
            None,
            Attached,
            Init,
            Ready,
            Failed
        }

        internal State _status;
        
        
        
        internal virtual void Init(){}

        internal virtual void Internal_Init(){}

    }
    
    public class T70_ServiceT<T> : T70_Service where T : T70_Service
    {
        //
        //    SINGLETON
        //
        static T _api;
        public static T Api { get { return _api; }}
        
        //
        //    ALLOW INIT:    must be static so can be set without Api being existed or not
        //
        private static bool _allowInit = false;

        public static void AllowInit()
        {
            if (_allowInit)
            {
                // It's ok to call AllowInit multiple time, the earliest call win the race
                return;
            }

            _allowInit = true;
            if (_api == null) return;
            if (_api._status < State.Init) _api.Internal_Init();
        }
        
        
        //
        //    READY:    get to know if the service is ready or not, static so would be working before Api created
        //
        public static bool IsReady
        {
            get { return _api != null && _api._status == State.Ready; }
        }
        
        
        //
        //    READY CALLBACK:    callback when Api is Ready
        //
        protected static event Action OnReadyCB;

        public static void OnReady(Action cb)
        {
            if (cb == null)
            {
                Debug.LogWarning("Callback should not be null!");
                return;
            }

            if (IsReady)
            {
                cb();
                return;
            }

            OnReadyCB -= cb;
            OnReadyCB += cb;
        }
        
        // ------------------------- instance ---------------------------
        
        protected virtual void Awake()
        {
            if (_api != null && _api != this)
            {
                Destroy(this);
                return;
            }

            _api = (T)(T70_Service)this;
            _status = State.Attached;
            
            DontDestroyOnLoad(this);

            if (!_allowInit) return;
            Internal_Init();
        }
        
        internal void Internal_SetReady(bool isReady)
        {
            // something wrong?
            if (_api == null) return;
            if (this == null) return;
            
            if (_status != State.Init)
            {
                #if UNITY_EDITOR
                Debug.LogWarning("[Editor] Expecting status [ Init ]");
                #endif
                
                return;
            }

            _status = isReady ? State.Ready : State.Failed;

            if (!isReady) return; // do not trigger OnReady
            if (OnReadyCB == null) return;
            
            var cb = OnReadyCB;
            OnReadyCB = null;
            cb();
        }
        
        internal override void Internal_Init()
        {
            if (!_allowInit)
            {
                #if UNITY_EDITOR
                Debug.LogWarning("[Editor] Internal_Init() should only be called when _allowInit = true");
                #endif
                
                return;
            }
            
            if (_status >= State.Init)
            {
#if UNITY_EDITOR
                Debug.LogWarning("[Editor] Init triggered before!");
#endif
                return;
            }

            _status = State.Init;
            Init();
        }
    }
}