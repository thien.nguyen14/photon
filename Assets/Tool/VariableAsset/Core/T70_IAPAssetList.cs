﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using T70.VariableAsset;


[CreateAssetMenu(fileName = "iap.asset.list.asset", menuName = "Variable Asset/Game/IAP Asset List")]
public class T70_IAPAssetList : ListAsset<IAPItem, T70_IAPAsset>
{
}
