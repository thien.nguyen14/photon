﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using T70.VariableAsset;
using System;


public class T70_IAPAsset : AssetT<IAPItem>
{

}
[Serializable]
public class IAPItem
{
    public string ID;
    public string DisplayName;
    public string Android_ID;
    public string IOS_ID;
    public bool Consumable;
    public float DefaultPrice;
    public List<RewardConfig> rewards;
    public ProductType productType;
    public bool isRewardAd;

    public string getProductId()
    {
#if UNITY_ANDROID
        return Android_ID;
#endif
        return IOS_ID;
    }

    public enum ProductType
    {
        Consumable = 0,
        NonConsumable = 1,
        Subscription = 2
    }
}
[Serializable]
public class RewardConfig
{
    public RewardType Type;
    public int Value;
}
public enum RewardType
{
    RemoveAds,
    Diamond,
    VipSubscription
}
