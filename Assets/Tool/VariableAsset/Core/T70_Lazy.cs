using System;
using UnityEngine;

namespace T70
{
    public class T70_LazyT<T>
    {
        protected Func<T> _eval;
        protected T _cache;

        private float timeStamp;
        private float cacheDuration;
		
        public T70_LazyT(Func<T> p_eval, T p_value, float cacheDuration = 5f)
        {
            _cache = p_value;
            _eval = p_eval;
            this.cacheDuration = cacheDuration;
        }

        public T ForceGet()
        {
            timeStamp = Time.realtimeSinceStartup;
            _cache = _eval();
            return _cache;
        }
		
        public static implicit operator T(T70_LazyT<T> d)
        {
            var needRefresh = Time.realtimeSinceStartup - d.timeStamp >= d.cacheDuration;
            return needRefresh ? d.ForceGet() : d._cache;
        }
    }
    
    internal class LazyBool : T70_LazyT<bool>
    {
        public LazyBool(Func<bool> p_eval, bool p_value, float cacheDuration = 5f) : base(p_eval, p_value, cacheDuration) 
        {}
    }
}