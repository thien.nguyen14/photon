﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class T70_Debug
{
	#if UNITY_EDITOR
	static Dictionary<string, HashSet<string>> map = new Dictionary<string, HashSet<string>>();
	static public void EditorLogWarningOnce(string category, string key, string message)
	{
		HashSet<string> h;

		if (!map.TryGetValue(category, out h))
		{
			h = new HashSet<string>();
			map.Add(category, h);

			h.Add(key);
			Debug.LogWarning("[Editor] "+ message);
			return;
		}

		if (h.Contains(key)) return;
		h.Add(key);
		Debug.LogWarning("[Editor] "+ message);
	}
	#endif

	public static void EditorLog(string message,LogType logType = LogType.Log)
	{
#if UNITY_EDITOR
		switch (logType)
		{
			case LogType.Warning:
				Debug.LogWarning("[Editor] " + message);
				break;
			case LogType.Error:
				Debug.LogError("[Editor] " + message);
				break;
			default:
				Debug.Log("[Editor] " + message);
				break;
		}
#endif
	}
}
