﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class T70_Resources : MonoBehaviour
{
    public static T Load<T>(string path) where T: UnityEngine.Object
    {
        return Resources.Load<T>(path);
    }


    public class P
    {
        const string CACHE_PATH = "files";
        public static string Embed(string id) { return id; }
        

        static string _local; // cache
        static internal void GetLocal()
        {
            #if UNITY_EDITOR
            _local = Path.Combine(Application.dataPath.Replace("Assets", string.Empty), "Library/", CACHE_PATH);
            #endif
            _local = Path.Combine(Application.persistentDataPath, CACHE_PATH);

            // make sure all splash are valid
            _local = _local.Replace("\\", "/");
            
            // make sure path ends with /
            if (_local[_local.Length -1] != '/') _local += "/";
        }
        
        public static string LocalFileIO(string id) 
        {
            if (string.IsNullOrEmpty(_local)) GetLocal();
            return _local + id;
        }

        public static string LocalWWW(string id) 
        {
            if (string.IsNullOrEmpty(_local)) GetLocal();
            return "file://" + _local + id;
        }
    }
    
    public class Res
    {
        public string name;
        public int version;
        
        internal WeakReference dataRef;


        [System.NonSerialized] string _id;
        public string id { get { 
            if (!string.IsNullOrEmpty(_id)) return _id;
            _id = string.Format("{0}.{1}", name, version);
            return _id;
        }}
        
        public string GetURL(string basePath)
        {
            var chr = (basePath[basePath.Length-1] == '/') ? string.Empty : "/";
            return basePath + id;
        }
        
        public T TryGet<T>() where T : class // AssetClass
        {
            if (dataRef == null) return null;
            if (!dataRef.IsAlive) return null;
            return (T)dataRef.Target;
        }

        public bool SetCache<T>(T target) where T : class
        {
            if (dataRef != null && dataRef.IsAlive)
            {
                Debug.LogWarning("Weak References existed & alive - you may have tried to load multiple instances of this asset at the same time!");
                return false;
            }

            dataRef = new WeakReference(target, false);
            return true;
        }
    }

    public class ResCollection
    {
        public List<Res> list;
    }

    public class ResMap
    {
        [SerializeField] public ResCollection embed;
        public ResCollection local;
    }

    


    public static T70_Resources _api;
    static public T70_Resources Api { get { return _api; } }


    // ----------------- Instance --------------------

    
    

    void Awake()
    {
        if (_api != null && _api != this) {
            Destroy(this);
            return;
        }

        _api = this;
        DontDestroyOnLoad(this);
    }

    bool ValidateRes<T>(Res res) where T : class
    {
        if (res == null)
        {
            Debug.LogWarning("res to load should not be null!");
            return false;
        }

        var obj = res.TryGet<T>();
        if (obj != null)
        {
            Debug.LogWarning("Res has been loaded & existed in cache! " + res.id);
            return false;
        }

        return true;
    }
    string ValidateLocalCache(Res res)
    {
        var localPath = P.LocalFileIO(res.id);
        
        // Check if file is actually existed locally
        var isExisted = false;
        try 
        {
            isExisted = File.Exists(localPath);
        } catch (Exception e)
        {
            Debug.LogWarning("Error checking file exist: " + res.id + "\n" + e);
        }

        return isExisted ? localPath : null;
    }

    void SetWWWCache<T>(Res res, WWW www)
    {
        var typeT = typeof(T);

        if (typeT == typeof(string))
        {
            res.SetCache<string>(www.text); return;
        }

        if (typeT == typeof(byte[]))
        {
            res.SetCache<byte[]>(www.bytes); return;
        }

        if (typeT == typeof(Texture2D))
        {
            res.SetCache<Texture2D>(www.texture); return;
        }

        if (typeT == typeof(AudioClip))
        {
            res.SetCache<AudioClip>(www.GetAudioClip()); return;
        }
        
        Debug.LogWarning("Unsupported type: " + typeT);
    }

    IEnumerator TryLoadEmbed(Res res)
    {
        if (!ValidateRes<UnityEngine.Object>(res)) yield break;

        res.dataRef = null;
        var obj = Resources.Load(P.Embed(res.id));
        if (obj != null) res.SetCache<UnityEngine.Object>(obj);
    }

    IEnumerator TryLoadLocalFileIO(Res res)
    {
        if (!ValidateRes<byte[]>(res)) yield break;

        var localPath = ValidateLocalCache(res);
        if (string.IsNullOrEmpty(localPath)) yield break;
        
        // Do load
        res.dataRef = null;
        byte[] data = null;
        try 
        {
            data = File.ReadAllBytes(localPath);
        } catch (Exception e)
        {
            Debug.LogWarning("Error while reading file: " + res.id + "\n" + e);
            yield break;
        }
        
        if (data != null && data.Length > 0) res.SetCache(data);
    }

    IEnumerator TryLoadLocalWWW<T>(Res res) where T : UnityEngine.Object
    {
        var localPath = ValidateLocalCache(res);
        if (string.IsNullOrEmpty(localPath)) yield break;

        var localURL = P.LocalWWW(res.id);
        var loader = new WWW(localURL);

        while(!loader.isDone)
        {
            if(!string.IsNullOrEmpty(loader.error))
            {
                break;
            }

            yield return null;
        }

        if (!string.IsNullOrEmpty(loader.error))
        {
            Debug.LogWarning("download error: " + loader.error + "  url: " + localURL);
            yield break;
        }
        
        SetWWWCache<T>(res, loader);
    }
    
    
    #if UNITY_EDITOR
    [ContextMenu("Refresh Embeded Resources")]
    void RefreshEmbedResources()
    {

    }
    #endif
}

