﻿using com.team70;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InteractionEffect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    public static float timeDelayDoEffect = 0.1f;
    
    public enum Type
    {
        None,
        Scale
    }

    public Type effectType = Type.Scale;
    public Vector3 targetScale = new Vector3(0.9f, 0.9f, 0.9f);
    public float duration = 0.15f;

    private Vector3 originScale = Vector3.one;
    private Tween currentTween = null;
    private bool doingEffect = false;
    private string _doEffectId;

    public Button btn;
    
    private void Awake() 
    {
        originScale = transform.localScale;
        _doEffectId = $"DoEffect_{gameObject.GetInstanceID()}";
    }

    private void OnEnable() 
    {
        transform.localScale = originScale;
        doingEffect = false;

        if (currentTween != null)
        {
            currentTween.Kill();
            currentTween = null;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Async.Call(DoEffect, timeDelayDoEffect, _doEffectId);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Async.Kill(_doEffectId);
        ResetEffect();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ResetEffect();
    }

    private void DoEffect()
    {
        if (btn != null && btn.interactable == false) return;
        if (effectType == Type.None) return;

        doingEffect = true;

        if (effectType == Type.Scale)
        {
            currentTween = transform.DOScale(targetScale, duration);
            return;
        }
    }

    private void ResetEffect()
    {
        if (btn != null && btn.interactable == false) return;
        if (doingEffect == false) return;
        if (effectType == Type.None) return;

        if (currentTween != null)
        {
            currentTween.Kill();
            currentTween = null;
        }

        if (effectType == Type.Scale)
        {
            transform.DOScale(originScale, duration);
        }

        doingEffect = false;
    }
}
