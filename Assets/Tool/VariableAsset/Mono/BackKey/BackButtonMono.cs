﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using T70.VariableAsset;
public class BackButtonMono : MonoBehaviour
{
    public BackButtonAsset backButtonAsset;

    private void Update()
    {
        if (backButtonAsset == null) return;

        if(Input.GetKeyUp(KeyCode.Escape))
        {
            backButtonAsset.OnKeyBack();
        }

    }

}
