﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using T70.VariableAsset;
using System;

public enum UI_Type {
    Screen,
    Popup
}
[CreateAssetMenu(fileName = "back.button.asset", menuName = "Variable Asset/Game/Back Button")]
public class BackButtonAsset : AssetT<Action>
{


    List<Action> lstPopupCallBack = new List<Action>();
    List<Action> lstScreenCallBack = new List<Action>();

    public void OnKeyBack()
    {
        if(lstPopupCallBack.Count > 0)
        {
            lstPopupCallBack[lstPopupCallBack.Count - 1].Invoke();
            Debug.Log("Invole lstPopupCallBack");
            return;
        }

        if (lstScreenCallBack.Count <= 0) return;
        Debug.Log("Invole lstScreenCallBack");
        lstScreenCallBack[lstScreenCallBack.Count - 1].Invoke();
    }


    public void RegisterBackButtonEvent(UI_Type type, GameObject target, Action callback)
    {
        if(type == UI_Type.Popup)
        {

            lstPopupCallBack.Add(callback);
        }
        else
        {
            lstScreenCallBack.Add(callback);
        }
    }
    public void RemoveCallBack(UI_Type type, GameObject target, Action callback)
    {

        // We need to remove exactly callback, 
        // NOT in top of list because some back action add another action to list

        if (type == UI_Type.Popup)
        {
            lstPopupCallBack.Remove(callback); 
            //lstPopupCallBack.RemoveAt(lstPopupCallBack.Count -1);
        }
        else
        {
            lstScreenCallBack.Remove(callback);
            //lstScreenCallBack.RemoveAt(lstScreenCallBack.Count - 1);
        }
    }

    //class CallBackData
    //{
    //    public GameObject gameObject;
    //    public Action
    //}
}
