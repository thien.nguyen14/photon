﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



#if UNITY_EDITOR
using System;
using UnityEditor;
using System.Reflection;
using UnityEditor.Events;
#endif

public class BackButtonHelper : MonoBehaviour
{
    public UI_Type type;

    public UnityEvent action;
    public BackButtonAsset backButton;

    private bool _inited;
    
    private void OnEnable()
    {
        if (_inited) backButton.RegisterBackButtonEvent(type, gameObject, OnBackButton);

    }
    private void OnBackButton()
    {
        action.Invoke();
    }
    private void OnDisable()
    {
        backButton.RemoveCallBack(type, gameObject, OnBackButton);
    }

    public void Init(UI_Type type, UnityAction callBack)
    {
        _inited = true;
        
        if (action == null) action = new UnityEvent();
        
        action.RemoveListener(callBack);
        action.AddListener(callBack);

        this.type = type;
        backButton = GlobalVA.backButton;
        
        if (this.gameObject.activeInHierarchy == false) return;
        
        backButton.RemoveCallBack(type, gameObject, OnBackButton);
        backButton.RegisterBackButtonEvent(type, gameObject, OnBackButton);
    }


#if UNITY_EDITOR
    static HashSet<string> CloseMethodName = new HashSet<string>
    {
        "close",
        "onclose"
    };
    private void Reset()
    {
        var assets = AssetDatabase.FindAssets("t:BackButtonAsset");
        if (assets.Length == 0) return;
        backButton = AssetDatabase.LoadAssetAtPath<BackButtonAsset>(AssetDatabase.GUIDToAssetPath(assets[0]));



        //try to search close component
        if(action == null)
        {
            action = new UnityEvent();
        }
        action.RemoveAllListeners();
        var comps = GetComponents<MonoBehaviour>();

        

        foreach(var comp in comps)
        {

            var type = comp.GetType();
            if (type == typeof(BackButtonHelper)) continue;

            Debug.Log("Type: " + type);

            var allPublicFunc = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            foreach(var method in allPublicFunc)
            {
                if (method.GetParameters().Length > 0) continue;
                if (method.ReturnType != typeof(void)) continue;

                var methodName = method.Name.ToLower();


                if (CloseMethodName.Contains(methodName))
                {
                    Debug.Log("add action close: " + methodName);

                    UnityAction methodDelegate = System.Delegate.CreateDelegate(typeof(UnityAction), comp, method.Name) as UnityAction;
                    UnityEventTools.AddPersistentListener(action, methodDelegate);

                    //UnityAction<string> methodDelegateString = System.Delegate.CreateDelegate(typeof(UnityAction<string>), comp, method.Name) as UnityAction<string>;
                    //UnityEventTools.AddStringPersistentListener(action, methodDelegateString, "lala");


                }
            }
        }


    }

#endif
}
