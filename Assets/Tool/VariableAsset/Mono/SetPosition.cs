﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using T70.VariableAsset;

public class SetPosition : MonoBehaviour
{
    public FloatAsset x;
    public FloatAsset y;
    public FloatAsset z;


    public V.Float offsetX;
    public V.Float offsetY;
    public V.Float offsetZ;

    public V.Float multiplierX;
    public V.Float multiplierY;
    public V.Float multiplierZ;
    
    [SerializeField] bool local;
    public Transform target;

    Vector3 pos;
    

    void Start()
    {
        if (target == null) target = transform;
        pos = local ? target.localPosition : target.position;
    }
    
    void Update()
    {
        if (x != null) pos.x = offsetX + x.Value * multiplierX;
        if (y != null) pos.y = offsetY + y.Value * multiplierY;
        if (z != null) pos.z = offsetZ + z.Value * multiplierZ;

        if (local)
        {
            target.localPosition = pos;
        } else {
            target.position = pos;
        }
    }

}
