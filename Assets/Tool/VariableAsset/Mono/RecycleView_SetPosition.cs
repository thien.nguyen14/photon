﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using T70.VariableAsset;

public class RecycleView_SetPosition : MonoBehaviour
{
    public GameStateAsset state;
    
    public RecycleView view;
    
    

    public FloatAsset position;
    public IntAsset intPosition;
    
    public V.Int minValue;
    public V.Float offset;
    public V.Float multiplier;


    // public IntAsset currentNode;
    // public Vector3Asset firstViewPos;
    

    void OnEnable()
    {
        state.AddListener(OnChangeState);
        if (intPosition != null) intPosition.AddListener(OnChangePosition);
        if (position != null) position.AddListener(OnChangePosition);
    }

    void OnChangeState(GameState s)
    {
        if (s == GameState.Tutorial || 
            s == GameState.LoadSong ||
            s == GameState.Revive)
        {
            view.Refresh(true);
        }
    }

    void OnDisable()
    {
        if (intPosition != null) intPosition.RemoveListener(OnChangePosition);
        if (position != null) position.RemoveListener(OnChangePosition);
    }

    void OnChangePosition(int value)
    {
        view.itemPosition = Mathf.Max(minValue, value + offset);
        //firstViewPos.Value = view.getPos(currentNode);
    }
    
    public void OnChangePosition(float value)
    {
        view.itemPosition = Mathf.Max(minValue, value * multiplier + offset);
    }
}
