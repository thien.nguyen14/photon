﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using T70.VariableAsset;
using UnityEngine.Serialization;

public class Colorize : MonoBehaviour
{
    public Color2 current;
    [FormerlySerializedAs("backgroundColor")] public Color2Asset color2Color;
    public SpriteRenderer[] top;
    public SpriteRenderer[] bottom;

    private void Start() {
        
    }

    void Update()
    {
        // top
        var topColor = Color.Lerp(current.Top, color2Color.Value.Top, Time.deltaTime);
        current.Top = topColor;

        for (int k = 0; k < top.Length; k++)
        {
            var a = top[k].color.a; // keep alpha
            topColor.a = a;
            top[k].color = topColor;
        }

        RenderSettings.fogColor = topColor;
        RenderSettings.fog = true;

        // bottom
        var botColor = Color.Lerp(current.Bottom, color2Color.Value.Bottom, Time.deltaTime);
        current.Bottom = botColor;

        for (int k = 0; k < bottom.Length; k++)
        {
            var a = bottom[k].color.a;
            botColor.a = a;
            bottom[k].color = botColor;
        }
    }
}
