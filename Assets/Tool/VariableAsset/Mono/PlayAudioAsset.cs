﻿using System.Collections;
using System.Collections.Generic;
using T70.VariableAsset;
using UnityEngine;

public class PlayAudioAsset : MonoBehaviour
{
    
    public AudioSource audioSource;
    public V.Bool config;
    
    void Start()
    {
        
    }

    public void Play()
    {
        if (audioSource == null) return;
        if (config != null && config.Value) audioSource.Play();
    }
}
