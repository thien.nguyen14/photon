﻿using System;
using System.Collections;
using System.Collections.Generic;
using T70.VariableAsset;
using UnityEngine;

public class ActiveOnEnable : MonoBehaviour
{
    public BoolAsset enableAsset;
    public bool invert;
    public GameObject[] targets;

    private void Awake() 
    {
        Apply(enableAsset.Value);
        enableAsset.AddListener(OnEnableChanged);
    }

    private void OnDestroy() 
    {
        enableAsset.RemoveListener(OnEnableChanged);
    }

    private void OnEnableChanged(bool active)
    {
        Apply(enableAsset.Value);
    }

    internal void Apply(bool value)
    {
        if (targets == null) return;

        var n = targets.Length;

        for (int i = 0; i < n; i++)
        {
            var go = targets[i];
            if (go == null) continue;
            go.SetActive(invert ? !value : value);
        }
    }
}
