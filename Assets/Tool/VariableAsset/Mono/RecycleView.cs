﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IRecycleItem
{
	void Reset(int index);
}

[Serializable] public class RItem 
{
	public GameObject go;
	public IRecycleItem handler;
	
    [NonSerialized] bool _inUse = false;
    [NonSerialized] bool _inited = false;

	internal int _index;
	public int Index {
		get { return _index; }
		set {
			if (_index == value) return;
			_index = value;
			Reset(true);
		}
	}

	void Init()
	{
		_inited = true;
		var monos = go.GetComponents<MonoBehaviour>();
		for (int i = 0;i < monos.Length;i ++)
		{
			if (monos[i] is IRecycleItem)
			{
				handler = (IRecycleItem)monos[i];
				return;
			}
		}

		//Debug.LogWarning("Handler not found! " + rect.gameObject + ":" + monos.Length);
	}

    public void Reset(bool inUse)
    {
        // Debug.Log(this.go + " --> " + inUse + " | " + _index);
        if (inUse == false)
        {
            go.SetActive(false);
            _inUse = false;
            return;
        }
		
        _inUse = true;
        go.SetActive(true);
		
        if (!_inited) Init();
        go.name = "_" + _index;
        if (handler == null) return;
        handler.Reset(_index);
    }
}

public class RecycleView : MonoBehaviour
{
    public bool refreshOnStart;

	[SerializeField] float uniSize;
	[SerializeField] internal float uniOffset;

	[SerializeField] public RItem[] pool;
	
	[Range(0, 1000)] public float _itemPosition;
	public bool logChange;
	
	public float itemPosition
	{
		get { return _itemPosition; }
		set {
			if (_itemPosition == value) return;

			if (logChange) Debug.Log("Changed: " + value);
			
			_itemPosition = value;
			Refresh();
		}
	}
	
	public float uniPosition
	{
		get { return _itemPosition * uniSize + uniOffset; }
		set {
			itemPosition = (value - uniOffset) / uniSize;
		}
	}

	internal int _maxIndex = -2;
	public void SetMaxIndex(int max)
	{
		_maxIndex = max;
	}
	
	#if UNITY_EDITOR

	// float _lastCellPosition;
	// void OnValidate()
	// {
	// 	Debug.Log("ON VALIDATE!");

		// DO NOT COMMENT OUT THIS LINE
		// THIS IS TO ENSURE EDITOR & RUNTIME HAS THE SAME EXPERIENCE
		//if (Application.isPlaying) return;
		
	// 	if (_itemPosition != _lastCellPosition)
	// 	{
	// 		_lastCellPosition = _itemPosition;
	// 		DelayRefresh();
	// 	}
	// }

	void DelayRefresh()
	{
		UnityEditor.EditorApplication.update -= Refresh;
		UnityEditor.EditorApplication.update += Refresh;
	}

	[ContextMenu("GetChildren")]
	void GetChildren()
	{
		var list = new List<RItem>();
		
		foreach (Transform c in transform)
		{
			if (c == transform) continue;
			
			list.Add(new RItem()
			{
				go = c.gameObject
			});
		}
		
		pool = list.ToArray();
		uniSize = pool.Length;
		DelayRefresh();

		UnityEditor.EditorUtility.SetDirty(this);
	}
#endif

	public void SetupPool(GameObject prefab, int count, bool reusePrefab = true)
	{
		var goList = new List<GameObject>();
		for (var i = 0; i < count; i++)
		{
			if (reusePrefab && i == 0)
			{
				goList.Add(prefab);
				continue;
			}
			
			var go = Instantiate(prefab);
			goList.Add(go);
		}
		
		SetupPool(goList);
	}
	
	public void SetupPool(List<GameObject> poolItems)
	{
		var list = new List<RItem>(poolItems.Count);
		
		for (var i = 0; i < poolItems.Count; i++)
		{
			list.Add(new RItem() { go = poolItems[i] });
		}
		
		pool = list.ToArray();
		uniSize = pool.Length;
	}

    // public void Reset()
    // {
    // 	var poolN = pool.Length;
    // 	for (int i = 0;i < poolN; i ++)
    // 	{
    // 		var poolI = pool[i];
    // 		if (poolI.go == null) continue;
    // 		if (poolI.handler != null)
    // 		{
    // 			poolI.handler.Reset(poolI.Index);
    // 		}
    // 	}
    // }

    private void Start()
    {
        if (refreshOnStart) Refresh(true);
    }

    void Refresh()
	{
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.update -= Refresh;
		#endif

		Refresh(false);
	}
	
	public void Refresh(bool forceTriggerHandlers)
	{
        if (this == null) return;

		var poolN = pool.Length;
		var clampPos = Mathf.Max(0, _itemPosition);
		var firstCell = (int)(clampPos);
		var firstPool = firstCell % poolN;

		// Debug.Log($"{this} Refresh: {forceTriggerHandlers} poolN = {poolN} : first={clampPos}");
		
		for (int i = 0;i < poolN; i ++)
		{
			var cellIdx = (firstCell + i);
			var poolIdx = (i + firstPool) % poolN;
			var poolI = pool[poolIdx];
			if (poolI == null || poolI.go == null) continue;

			// skip update if item passed _maxIndex
			if (_maxIndex != -2 && (_maxIndex < cellIdx))
			{
				poolI.Reset(false);
				continue;
			}
			
			if (forceTriggerHandlers)
			{
				// temporarily change _index so that next Index call will always trigger handler callback
				poolI._index = cellIdx-1;
			}

			poolI.Index = cellIdx;
		}
	}
    public Vector3 getPos(int index)
    {
        var poolN = pool.Length;
        for (int i = 0; i < poolN; i++)
        {
            var poolI = pool[i];
            if (poolI.go == null) continue;
            if(poolI.Index == index)
            {
                return poolI.go.transform.localPosition;
            }

        }
        return Vector3.zero;
    }

	public void InitPool(int size)
	{
		pool = new RItem[size];
		uniSize = size;
	}
}
