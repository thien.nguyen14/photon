﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using T70.VariableAsset;
using System;

public class DragInput : MonoBehaviour
{
	enum Phase
	{
		None, Start, Update, End
	}

    public BoolAsset isTouch;

    public V.Float sensitive;
    public V.Float physicSizeFactor;
    
    public FloatAsset outputX;
    public FloatAsset outputY;

    [Header("Limit sensitive")]
    public BoolAsset useLimitSensitive;
    public FloatAsset limitSensitive;
	public FloatAsset xSpacing;
	public IntAsset nTracks;
	public FloatAsset limitXSpace;

    Vector2 startDragV;
    private float screenFactor = 1f;
	
	void OnStartDrag()
	{
		startDragV = new Vector2(outputX != null ? outputX.Value : 0, outputY != null ? outputY.Value : 0);
	}

	private float currentSensitive
	{
		get { return sensitive.Value * Mathf.Lerp(1f, screenFactor, physicSizeFactor.Value); }
	}
	
	void OnUpdateDrag()
	{
		var v = startDragV + dragDelta * (useLimitSensitive ? limitSensitive.Value : currentSensitive);
		var tracks = Math.Max(nTracks.Value, 2);
		var ratio = 1f;

		if (limitXSpace != null && limitXSpace.Value > 0) ratio = limitXSpace.Value;

		var space = (xSpacing * 1.8f * (tracks - 1) * ratio) / 2f;
		if (v.x < -space || v.x > space)
		{
			dragPhase = Phase.None;
			dragDelta = Vector2.zero;
			v.x = Mathf.Clamp(v.x, -space, space);
		}
		
		if (outputX != null) outputX.Value = v.x;
		if (outputY != null) outputY.Value = v.y;
	}
	
    private void Start()
    {
	    if (Screen.width == 0 || Screen.height == 0 || Screen.dpi == 0)
	    {
		    Debug.LogWarning("Unsupported screen - not enough info!");
		    screenFactor = 1f;
	    }
	    else
	    {
		    var iPhoneXW = 1125f / 458f; // iphoneX's width
		    var myW = Mathf.Min(Screen.width, Screen.height) / (float) Screen.dpi; // my width
		    screenFactor = myW / iPhoneXW;    
		    
		    //Debug.Log("Screen factor: " + screenFactor);
	    }
	    
        useLimitSensitive.AddListener(OnLimitChanged);
    }

    private void OnLimitChanged(bool obj)
    {
        dragDelta = Vector2.zero;
        dragPhase = Phase.End;
        dragPhase = Phase.None;
    }


    /// ----------------

    // SCREEN INFO
    Vector2 startTouchPos;
	Vector2 lastTouchPos;
	Vector2 dragDelta;
	Phase dragPhase;
	
	public void ForceEnd()
	{
		if (outputX != null) outputX.Value = 0f;
		if (outputY != null) outputY.Value = 0f;
		dragDelta = Vector2.zero;
		dragPhase = Phase.End;
		dragPhase = Phase.None;
	}
	
	public void Update()
    {  
		const int TOUCH_ID = 0;

		#if UNITY_EDITOR
		if (Input.GetMouseButton(TOUCH_ID))
		{
			var tp = Input.mousePosition;
#else
		if (Input.touchCount > 0) 
		{
			var tp = Input.GetTouch(TOUCH_ID).position;
#endif

            if (dragPhase == Phase.None)
			{
				dragPhase = Phase.Start;
				startTouchPos = tp;
				lastTouchPos = tp;
				OnStartDrag();
			} else {
				dragPhase = Phase.Update;
			}
            
			lastTouchPos = tp;
			var v = lastTouchPos - startTouchPos;
			dragDelta = v / Screen.width;

			if (dragPhase == Phase.Update) 
			{
				OnUpdateDrag();
			}

		} else {
			if (dragPhase == Phase.Update || dragPhase == Phase.Start)
			{
				dragDelta = Vector2.zero;
				dragPhase = Phase.End;
				dragPhase = Phase.None;
			}
		}

#if UNITY_EDITOR

        isTouch.Value = Input.GetMouseButton(0);
#else
        isTouch.Value = Input.touchCount > 0;
#endif


    }

}
