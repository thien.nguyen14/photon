﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshSlide : MonoBehaviour
{
    public MeshFilter meshFilter;
    public Mesh originMesh;

    public Vector3 offset = Vector3.one;
    public Vector3 size = Vector3.one;

    private Mesh modifiedMesh;
    private Bounds bounds;
    private Vector3 minOffset, maxOffset;

    private Vector3 prevOffset = Vector3.one;
    private Vector3 prevSize = Vector3.one;

    #if UNITY_EDITOR
    private void OnValidate() 
    {
        if (meshFilter == null) 
        {
            meshFilter = gameObject.GetComponent<MeshFilter>();
            originMesh = meshFilter.sharedMesh;
        } 
    }

    void OnDrawGizmosSelected()
    {
        UpdateOffsetSize();
        
        var wPos = transform.TransformPoint(bounds.center);
        var wMax = transform.TransformPoint(maxOffset);
        var wMin = transform.TransformPoint(minOffset);

        var gizmosSize = (wMax - wMin);

        Gizmos.color = new Color(1, 0, 1, 0.5f);
        Gizmos.DrawWireCube(wPos, gizmosSize);
    }
    #endif

    void UpdateOffsetSize()
    {
        bounds = originMesh.bounds;
        var offsetSize = new Vector3(bounds.size.x * offset.x * 0.5f, bounds.size.y * offset.y * 0.5f, bounds.size.z * offset.z * 0.5f);

        minOffset = bounds.center - offsetSize;
        maxOffset = bounds.center + offsetSize;
    }

    [ContextMenu("Reset Mesh")]
    public void ResetMesh()
    {
        if (originMesh == null) 
        {
            #if UNITY_EDITOR
            Debug.Log("[Editor] Should set originMesh!");
            #endif
            return;
        }

        if (!IsDirty()) return;

        modifiedMesh = null;
        meshFilter.mesh = originMesh;

        UpdateOffsetSize();

        #if UNITY_EDITOR
        // Debug.Log("vertices: " + originMesh.vertexCount);
        // Debug.Log("bounds: " + bounds + " size: " + bounds.size);
        // Debug.Log("minOffset: " + minOffset + " maxOffset: " + maxOffset);
        #endif
    }

    public void UpdateSizeX(float x)
    {
        size.x = x;
        UpdateSize();
    }

    [ContextMenu("Update Mesh")]
    public void UpdateSize()
    {
        if (originMesh == null) return;

        if (!IsDirty()) return;

        if (modifiedMesh == null)
        {
            modifiedMesh = new Mesh();
        }

        var newVertices = originMesh.vertices;
        var colors = originMesh.colors;
        var triangles = originMesh.triangles;
        var normals = originMesh.normals;
        var uv = originMesh.uv;

        for (int i = 0; i < newVertices.Length; i++)
        {
            var vertex = newVertices[i];

            // todo: check for y, z 
            if (vertex.x < minOffset.x)
            {
                var offsetDist = (vertex.x - minOffset.x);
                vertex.x = minOffset.x * size.x + offsetDist;
                newVertices[i] = vertex;
                continue;
            }

            if (vertex.x > maxOffset.x)
            {
                var offsetDist = (vertex.x - maxOffset.x);
                vertex.x = maxOffset.x * size.x + offsetDist; 
                newVertices[i] = vertex;
                continue;
            }
            
            newVertices[i] = vertex;
        }

        modifiedMesh.vertices = (newVertices);
        modifiedMesh.triangles = triangles;
        modifiedMesh.colors = colors;
        modifiedMesh.normals = normals;
        modifiedMesh.uv = uv;
        meshFilter.mesh = modifiedMesh;

        prevOffset = offset;
        prevSize = size;
    }

    private bool IsDirty()
    {
        return true;
    }
}
