﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayOnActive : MonoBehaviour
{
    public List<ParticleSystem> targets = new List<ParticleSystem>();
    
    private void Awake() 
    {
        
    }

    private void OnEnable() 
    {
        for (int i = 0; i < targets.Count; i++)
        {
            targets[i].Play();
        }
    }
}
