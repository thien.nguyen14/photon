﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using T70.VariableAsset;


namespace T70.VariableAsset
{
    public class SetActive : MonoBehaviour
    {
        public bool isActive;
        public GameObject[] targets;
        
        public void Active() { Apply(true); }
        public void Deactive() { Apply(false); }
        public void Toggle()
        {
            Apply(!isActive);
        }

        internal void Apply(bool value)
        {
            isActive = value;
            if (targets == null) return;

            var n = targets.Length;

            for (int i = 0;i < n; i++)
            {
                var go = targets[i];
                if (go == null) continue;
                go.SetActive(value);
            }
        }
    }

}
