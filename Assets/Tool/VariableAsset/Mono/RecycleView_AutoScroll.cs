﻿using System;
using System.Collections;
using System.Collections.Generic;
using T70.VariableAsset;
using UnityEngine;

public class RecycleView_AutoScroll : MonoBehaviour
{
    public RecycleView_SetPosition viewPosition;
    public GameStateAsset state;
    public FloatAsset position;
    public V.Float speed;

    private float autoPosition = 0;
    private bool active = false;
    private Vector3 originPosition = Vector3.zero;

    private void OnEnable() 
    {
        state.AddListener(OnChangeState);

        active = (state.Value == GameState.Menu);
        autoPosition = position;
        originPosition = transform.position;
    }

    private void OnDisable() 
    {
        state.RemoveListener(OnChangeState);
        active = false;
    }

    private void Update() 
    {
        if (!active) return;

        autoPosition += speed * Time.deltaTime;
        viewPosition.OnChangePosition(autoPosition);

        var current = transform.position;
        current.z -= speed * Time.deltaTime;
        transform.position = current;
    }

    private void OnChangeState(GameState s)
    {
        autoPosition = position;
        active = (s == GameState.Menu);
        if (!active)
        {
            transform.position = originPosition;
            viewPosition.OnChangePosition(position);
        }
    }
}
