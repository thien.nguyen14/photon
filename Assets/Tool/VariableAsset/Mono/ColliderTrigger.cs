﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using T70.VariableAsset;

using UnityEvent = UnityEngine.Events.UnityEvent;

public class ColliderTrigger : MonoBehaviour
{
    [Serializable] public class Info
    {
        public string tag;
        public string eventName;
        public StringAsset trigger;
    }
    
    public UnityEvent callback;
    
    public List<Collider> deactivate;
    public List<GameObject> activate;
    public List<Info> infos;
    
    public bool resetOnEnable = false;
    public bool updateX = false;

    private void OnEnable() 
    {
        if (resetOnEnable)
        {
            for (int i = 0; i < deactivate.Count; i++)
            {
                deactivate[i].enabled = true;
            }

            for (int i = 0; i < activate.Count; i++)
            {
                activate[i].SetActive(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        ForceHit(other);
    }
    public void ForceHit(Collider other)
    {
        if (callback != null) callback.Invoke();

        // Debug.LogWarning("OnTriggerEnter: " + this + " ---> " + other);

        foreach (var item in infos)
        {
            if (!other.CompareTag(item.tag))
            {
                //Debug.Log("Unmatched: " + other.tag + ":" + item.tag);
                continue;
            }

            if (item.trigger == null)
            {
                Debug.LogWarning("No trigger for tag <" + item.tag + ">");
                continue;
            }

            //Debug.Log("Hit! " + other.transform.parent);
            item.trigger.TriggerChange();
            break;
        }

        for (int i = 0; i < deactivate.Count; i++)
        {
            deactivate[i].enabled = false;
        }

        for (int i = 0; i < activate.Count; i++)
        {
            activate[i].SetActive(true);
            if (other != null && updateX)
            {
                var pos = activate[i].transform.position;
                pos.x = other.gameObject.transform.position.x;

                activate[i].transform.position = pos;
            }
        }
    }
}
