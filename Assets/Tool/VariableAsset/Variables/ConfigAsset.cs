using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using UnityEngine;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace T70.VariableAsset
{
    [CreateAssetMenu(fileName = "Config Asset", menuName = VariableConst.MenuPath.Config, order = VariableConst.Order.Config)]
    public class ConfigAsset : AssetT<List<Asset>>
    {
        private Dictionary<string, Asset> _dictConfig;
        public Dictionary<string, Asset> DictConfig
        {
            get
            {
                if (_dictConfig != null) return _dictConfig;
                _dictConfig = Value.ToDictionary(x => getAssetVariableName(x), x => x);
                return _dictConfig;
            }
        }

        private string getAssetVariableName(Asset asset)
        {
            if (asset == null)
            {
                Debug.LogError("Asset null");
                return string.Empty;
            }
            return asset.getAssetVariableName();
        }
        
        [ContextMenu("Debug Name")]
        private void DebugAllName()
        {
            foreach(var item in Value)
            {
                Debug.Log(getAssetVariableName(item));
            }
        }

        public void FromDictionary(Dictionary<string, string> data)
        {
            Dictionary<string, Asset> dictAsset = new Dictionary<string, Asset>();
            for (int i = 0; i < Value.Count; i++)
            {
                if (Value[i] == null)
                {
                    Debug.LogError("Asset is null in index: " + i);
                    continue;
                }
                
                var key = getAssetVariableName(Value[i]);
                if (!dictAsset.ContainsKey(key))
                {
                    dictAsset.Add(key, Value[i]);
                }
            }
            
            foreach (var item in data)
            {
                Asset asset = null;
                if (!dictAsset.TryGetValue(item.Key, out asset))
                {
#if UNITY_EDITOR
                    Debug.LogWarningFormat("[Editor] Asset with name {0} not found", item.Key);
#endif
                    continue;
                }
                asset.FromString(item.Value);
            }
        }

        public override void FromCSV(string csv)
        {
            var data = new Dictionary<string, string>();
            foreach (var item in XReader.GetHorizontalData(csv))
            {
                data[item.Key] = item.Value;
            }

            var countryData = PreProcessCountryData(data, DetectCountry.GetPhoneCountry());
            FromDictionary(countryData);
        }
        
        public Dictionary<string, string> PreProcessCountryData(Dictionary<string, string> data, string country)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            Dictionary<string, Dictionary<string, string>> dictCountryValue = new Dictionary<string, Dictionary<string, string>>();  //<variable name, <country, value>>
            foreach(var item in data)
            {
                var vName = item.Key;
                int index = vName.IndexOf('-'); //country delimiter index
                string vCountry = DetectCountry.DEFAULT_COUNTRY;
                if(index > 0)//have country code
                {
                    vCountry = vName.Substring(0, index);
                    vName = vName.Substring(index + 1);
                }
                if(!dictCountryValue.ContainsKey(vName))
                {
                    dictCountryValue.Add(vName, new Dictionary<string, string>());
                }
                dictCountryValue[vName].Add(vCountry, item.Value);
            }

            foreach(var item in dictCountryValue)
            {
                var val = string.Empty;
                var dict = dictCountryValue[item.Key];

                if(!dict.TryGetValue(country, out val))
                {
                    if(!dict.TryGetValue(DetectCountry.DEFAULT_COUNTRY, out val))
                    {
                        Debug.LogWarning("Cannot find value for country " + country + " in variable: " + item.Key);
                        continue;
                    }
                }
                result.Add(item.Key, val);
            }

            return result;
        }

		#if UNITY_EDITOR
        [ContextMenu("PolishAssets")]
        public void PolishAssets()
        {
            HashSet<Asset> hash = new HashSet<Asset>();

            List<Asset> list = this.Value;
            
            for (int i = list.Count - 1; i >= 0; i--)
            {
                Asset item = list[i];
                if (item == null || hash.Contains(item))
                {
                    list.RemoveAt(i);
                    continue;
                }
                
                hash.Add(item);
            }
            
            list.Sort((item1, item2) => String.Compare(item1.name, item2.name, StringComparison.Ordinal));
            
            EditorUtility.SetDirty(this);
        }
        #endif
    }
}