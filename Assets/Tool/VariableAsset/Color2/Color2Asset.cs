﻿using System;
using UnityEngine;

[Serializable]
public class Color2
{
    public Color Top;
    public Color Bottom;
}

namespace T70.VariableAsset
{
    [CreateAssetMenu(fileName = "Background Asset", menuName = VariableConst.MenuPath.Background_Asset, order = VariableConst.Order.Background_Asset)]
	public class Color2Asset : AssetT<Color2>
	{
        #if UNITY_EDITOR
		void Awake()
		{
			if (Value.Bottom == new Color(0,0,0,0)) Value.Bottom = Color.white;
            if (Value.Top == new Color(0,0,0,0)) Value.Top = Color.white;
		}
		#endif
	}
}
