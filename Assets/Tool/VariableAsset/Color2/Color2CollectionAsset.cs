using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace T70.VariableAsset
{
    [CreateAssetMenu(fileName = "Color2 collection", menuName = VariableConst.MenuPath.Color2_Collection, order = VariableConst.Order.Color2_Collection)]
    public class Color2CollectionAsset : CollectionAsset<Color2, Color2Asset>
    {
        [ContextMenu("Bind")]
        public override void Bind()
        {
            base.Bind();
        }
    }
}