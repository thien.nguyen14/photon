﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices;

public static class DetectCountry
{
    public static string DEFAULT_COUNTRY = "defaut";

#if UNITY_IOS
    [DllImport("__Internal")]
    private static extern string IOSgetPhoneCountryCode();
#endif

	public static string GetPhoneCountry()
	{
#if UNITY_EDITOR
        return DEFAULT_COUNTRY;
#elif UNITY_IOS
        return IOSgetPhoneCountryCode().ToLower();
#elif UNITY_ANDROID
        using (AndroidJavaClass cls = new AndroidJavaClass("java.util.Locale"))
        {
            using (AndroidJavaObject locale = cls.CallStatic<AndroidJavaObject>("getDefault"))
            {
                return locale.Call<string>("getCountry").ToLower();
            }
        }
#else
        return DEFAULT_COUNTRY;
#endif

    }
    public static bool IsChinaCountry
    {
        get 
        {
            var lc_country = GetPhoneCountry();
        
            bool isChinaCountry = (lc_country == "cn") || (lc_country == "chn");
            bool isChinese = (Application.systemLanguage ==  SystemLanguage.ChineseSimplified);
            return isChinaCountry || isChinese;
        }
    }
}
