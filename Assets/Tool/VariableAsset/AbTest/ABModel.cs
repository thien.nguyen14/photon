﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace T70.VariableAsset
{
    [Serializable]
    public class ABOption
    {
        public string name;
        public int weight;
        [NonSerialized] public int stWeight;

    }

    [Serializable]
    public abstract class ABModel
    {
        public string id;
        public string focus;
        public float percentUser;



        [NonSerialized] public string _current;

        public abstract bool Validate();
        public abstract string current { get; }

        public abstract string RandomOption();
        public abstract List<T> getListOption<T>();
        
    }


    [Serializable]
    public class ABModelT<T> : ABModel where T : ABOption
    {

        public List<T> options;



        [NonSerialized] private bool _readPP; // so that we read PlayerPref only once


        public override bool Validate()
        {
            //if (string.IsNullOrEmpty(id))
            //{
            //    Debug.LogWarning("ABModel id should not be null or empty!");
            //    return false;
            //}

            if (options == null || options.Count == 0)
            {
                Debug.LogWarning("ABModel options should not be empty!");
                return false;
            }

            var h = new HashSet<string>();
            for (int i = 0; i < options.Count; i++)
            {
                var op = options[i];

                if (string.IsNullOrEmpty(op.name))
                {
                    Debug.LogWarning("ABModel - option name should not be null or empty, index: " + i);
                    return false;
                }

                if (h.Contains((op.name)))
                {
                    Debug.LogWarning("ABModel - option name is duplicated <" + op.name + ">");
                    return false;
                }

                if (op.weight < 0)
                {
                    Debug.LogWarning("ABModel - option <" + op.name + "> has invalid weight: " + op.weight);
                    op.weight = 0;
                }

                h.Add(op.name);
            }

            if (!string.IsNullOrEmpty(focus))
            {
                if (!h.Contains(focus))
                {
                    Debug.LogWarning("ABModel - invalid focus option <" + focus + "> there are no option with that name!");
                    return false;
                }
            }

            return true;
        }



        public override string current
        {
            get
            {
                if (!string.IsNullOrEmpty(focus)) return focus;
                if (_readPP || !string.IsNullOrEmpty(_current)) return _current;

                _readPP = true;
                _current = PlayerPrefs.GetString("t70_ab." + id + ".ab", string.Empty);
                return _current;
            }
        }

        private int _totalWeight;
        private int TotalWeight
        {
            get
            {
                //if (_totalWeight > 0) return _totalWeight;

                _totalWeight = 0;

                for (int i = 0; i < options.Count; i++)
                {
                    var op = options[i];

                    op.stWeight = _totalWeight;
                    _totalWeight += op.weight;
                }

                return _totalWeight;
            }
        }

        private int RandomByWeight()
        {
            var rnd = Random.Range(0, TotalWeight * 1000) % TotalWeight;


            for (int i = 0; i < options.Count; i++)
            {
                var w = options[i].stWeight;
                if (rnd < w) return i - 1;
            }

            return options.Count - 1;
        }

        public override string RandomOption()
        {
            int rnd = RandomByWeight();

            var op = options[rnd];

            _current = op.name;
            _readPP = true; // redundancy but to make sure that we won't read PP
            PlayerPrefs.SetString("t70_ab." + id + ".ab", _current);
#if UNITY_EDITOR
            Debug.Log("[Editor] [AB TEST] Random option " + id + "  " + _current);
#endif
            return _current;
        }

        public override List<T1> getListOption<T1>()
        {
            return (List<T1>)(object)options;
        }
    }
}