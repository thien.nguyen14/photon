﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace T70.VariableAsset
{
    [CreateAssetMenu(fileName = "AbTest_MusicImpact", menuName = VariableConst.MenuPath.MusicImpact, order = VariableConst.Order.MusicImpact)]
    public class AB_MusicImpactAsset : ABAssetT<AB_MusicImpact.Model, AB_MusicImpact.Option>
    {
        public List<TextAsset> listDefaultConfig;
        public List<TextAsset> songListDefault;
    }

    public class AB_MusicImpact 
    {
        [System.Serializable]
        public class Option : ABOption
        {
            public string globalConfigUrl;
            public string songListUrl;
        }
        [System.Serializable]
        public class Model : ABModelT<Option>
        {

        }

    }
    
}