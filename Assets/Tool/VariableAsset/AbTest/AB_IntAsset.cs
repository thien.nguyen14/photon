﻿using System.Collections;
using System.Collections.Generic;
using T70.VariableAsset;
using UnityEngine;

[CreateAssetMenu(fileName = "AB_IntAsset", menuName = VariableConst.MenuPath.AB_IntAsset, order = VariableConst.Order.AB_IntAsset)]
public class AB_IntAsset : ABAssetT<AB_IntAsset.Model, AB_IntAsset.Option>
{
    public IntAsset targetConfig;

    public override void InitOption()
    {
        base.InitOption();
        targetConfig.Value = current.value;
    }

    [System.Serializable]
    public class Option : ABOption
    {
        public int value;
    }

    [System.Serializable]
    public class Model : ABModelT<Option>
    {

    }
}
