﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace T70.VariableAsset
{
    [CreateAssetMenu(fileName = "AbTest_Normal", menuName = VariableConst.MenuPath.AbTest_Normal, order = VariableConst.Order.AbTest_Normal)]
    public class AB_NormalAsset : ABAssetT<AB_Normal.Model, AB_Normal.Option>
    {
    }
    public class AB_Normal
    {
        [System.Serializable]
        public class Option : ABOption
        {
        }
        [System.Serializable]
        public class Model : ABModelT<Option>
        {

        }

    }

}