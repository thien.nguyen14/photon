﻿
using System;
using UnityEngine;
using System.Collections.Generic;

namespace T70.VariableAsset
{
    public interface IABTestInfo
    {
        string ID { get; }

        void FromJson(string json);
        void InitOption();
        void RandomOption();
        ABModel model { get; }
        float stWeight { get; set; }
    }

    public class ABAssetT<TModel, TOption> : AssetT<TModel>, IABTestInfo where TModel: ABModel where TOption : ABOption
    {
        public string id;
        public string ID { get { return id; } }
        public IntAsset option;
        public float stWeight { get; set; }
        public List<TOption> options
        {
            get
            {
                return Value.getListOption<TOption>();
            }
        }
        public override void FromJson(string json)
        {
            //Debug.Log("====" + json);
            var m = UnityEngine.JsonUtility.FromJson<TModel>(json);
            //Debug.Log((m == null) + "  " + typeof(TModel));
            Value = m;
            Value.Validate();
        }
        public ABModel model
        {
            get { return Value; }
        }
        public void RandomOption()
        {
            Value.RandomOption();
        }


        public virtual void InitOption()
            {
            option.Value = 0;

            var cur = Value.current;
            if (string.IsNullOrEmpty(cur)) return;

            for (int i = 0; i < options.Count; i++)
            {
                if (options[i].name == cur)
                {
                    option.Value = i;
                }
            }
            if (option < 0 || option >= options.Count)
            {
                return;
            }
        }

        public TOption current
        {
            get
            {
                //option.Value = 4;
                return options[option];
            }
        }
    }
}
