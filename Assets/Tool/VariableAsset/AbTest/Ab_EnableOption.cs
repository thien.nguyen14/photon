﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace T70.VariableAsset
{
    [CreateAssetMenu(fileName = "AbTest_EnableOption", menuName = VariableConst.MenuPath.AbTest_EnableOption, order = VariableConst.Order.AbTest_Normal)]
    public class Ab_EnableOption : ABAssetT<AB_Enable.Model, AB_Enable.Option>
    {
        public BoolAsset enableOption;
        public override void InitOption()
        {
            base.InitOption();

            enableOption.Value = option.Value != 0;

        }
    }
    public class AB_Enable
    {
        [System.Serializable]
        public class Option : ABOption
        {
        }
        [System.Serializable]
        public class Model : ABModelT<Option>
        {

        }

    }

}