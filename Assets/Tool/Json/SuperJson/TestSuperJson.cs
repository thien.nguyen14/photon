﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSuperJson : MonoBehaviour
{
    public TextAsset asset;

    void Start()
    {
        Partition();
    }

    [ContextMenu("Partition")]
    public void Partition()
    {
        nano.SuperJson.Partition(asset.text);
    }
}