﻿
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;

namespace nano
{
    public class SuperJson
    {
        public const string partition = "-----";

        public static string GetPartition(string pName, string sjson)
        {
            if (string.IsNullOrEmpty(pName))
            {
                return null;
            }
            
            if (string.IsNullOrEmpty(sjson))
            {
                Debug.LogWarning("SuperJson:SplitJsonSections() error - sjson should not be null or empty!");
                return null;
            }
            
            if (!sjson.StartsWith(partition, StringComparison.Ordinal))
            {
                Debug.LogWarning("SuperJson:SplitJsonSections() error - input might not be a partition json:\n" + sjson);
                return null;
            }

            var idx = sjson.IndexOf(pName, StringComparison.Ordinal);
            if (idx == -1) return null;
            
//            var idx1 = sjson.LastIndexOf(partition, idx, StringComparison.Ordinal);
//            if (idx1 == -1) return null;

            var idx1 = idx + pName.Length;
            var idx2 = sjson.IndexOf(partition, idx + pName.Length, StringComparison.Ordinal);
            if (idx2 == -1) idx2 = sjson.Length;
            
            return sjson.Substring(idx1, idx2-idx1);
        }
        
        public static Dictionary<string, string> Partition(string sjson)
        {
            var result = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(sjson))
            {
                Debug.LogWarning("SuperJson:SplitJsonSections() error - sjson should not be null or empty!");
                return result;
            }
            
            if (!sjson.StartsWith(partition, StringComparison.Ordinal))
            {
                Debug.LogWarning("SuperJson:SplitJsonSections() error - input might not be a partition json:\n" + sjson);
                return result;
            }
            
            var idx = 0;
            var label = string.Empty;
            var json = string.Empty;
            
            while (idx != -1)
            {
                var lb_st = idx + partition.Length;
                var lb_ed = lb_st + 1;

                while (lb_ed < sjson.Length)
                {
                    var lbc = sjson[lb_ed];
                    if (lbc == '\n' || lbc == '\r') break;
                    lb_ed++;
                }
                
                // label found!
                label = sjson.Substring(lb_st, lb_ed - lb_st).Trim();

                var js_st = lb_ed + 1;
                var js_ed = sjson.IndexOf(partition, js_st, StringComparison.Ordinal);
                
                if (js_ed == -1) //last one
                {
                    idx = -1; // ended!
                    js_ed = sjson.Length;
                }
                else
                {
                    idx = js_ed;
                }
                
                // json found!
                json = sjson.Substring(js_st, js_ed - js_st);
                if (result.ContainsKey(label))
                {
                    Debug.LogWarning("Duplicated partition found");
                }
                
                result.Add(label, json);
                //Debug.Log("Mapping: <" + label + "> ---> \n" + json);
            }
            
            return result;
        }
    }    
}


