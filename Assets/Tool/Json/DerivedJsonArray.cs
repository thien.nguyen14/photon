﻿using UnityEngine;
using System.Text;
using System.Collections.Generic;

namespace T70.Json
{
    public class DerivedJsonInfo
    {
        public string id;
        public string json;
    }
    public class DerivedJsonArray
    {
        const char BEGIN_OBJECT = '{';
        const char END_OBJECT = '}';
        const char END_ID_TAG = '\"';

        const string ID = "\"id\":";
        static readonly int ID_LENGTH = ID.Length;

        public static IEnumerable<DerivedJsonInfo> ReadJson(string json)
        {
            StringBuilder value = new StringBuilder();
            StringBuilder id = new StringBuilder();
            int DelimiterCount = 0;
            bool wasFoundObject = false;
            bool wasFoundId = false;
            bool wasOpenId = false;
            bool CompleteId = false;
            var length = json.Length;
            if (length < 2) yield break;

            int checkIdIndex = 0;

            for(int i = 1; i < length - 1; i++)//skip list
            {
                var c = json[i];
                if (c == BEGIN_OBJECT)
                {
                    wasFoundObject = true;
                    DelimiterCount++;
                }
                else if (c == END_OBJECT) DelimiterCount--;

                if (!wasFoundObject) continue;
                
                value.Append(c);

                #region Check ID
                if (!CompleteId)
                {
                    if (!wasFoundId)
                    {
                        //Debug.Log("Compare: " + c + " with " + ID[checkIdIndex]);
                        if (c == ID[checkIdIndex])
                        {
                            checkIdIndex++;
                            if (checkIdIndex >= ID_LENGTH - 1)//
                            {
                                //Debug.Log("Found");
                                wasFoundId = true;
                            }
                        }
                        else
                        {
                            checkIdIndex = 0;
                        }
                    }
                    else
                    {
                        if (c == END_ID_TAG)
                        {
                            if (!wasOpenId)
                            {
                                wasOpenId = true;
                            }
                            else
                            {
                                //Debug.Log("complete id");
                                CompleteId = true;
                            }

                        }
                        else if(wasOpenId)
                        {
                            //Debug.Log("id.Append(c) " + c);
                            id.Append(c);
                        }
                    }
                }
                #endregion


                if (DelimiterCount < 0)
                {
                    Debug.LogWarning("something wrong DelimiterCount < 0");
                    yield break;
                }
                if (DelimiterCount > 0) continue;

                yield return new DerivedJsonInfo
                {
                    id = id.ToString(),
                    json = value.ToString()
                };
                id.Length = 0;
                value.Length = 0;
                wasFoundObject = false;
                wasFoundId = false;
                CompleteId = false;
                wasOpenId = false;

            }
            
        }
    }
}