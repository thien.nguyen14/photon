﻿#define ENABLE_LOG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using T70.VariableAsset;
using UnityEngine;
using Random = UnityEngine.Random;

namespace nano
{
    [Serializable]
	public class AB
	{
		private const string PPKEY_DATA = "abv2.data";
		


		enum UserDataStatus
		{
			NotLoaded,
			NoData,
			DataLoaded,

			DataValidated,
			DataInvalid
		}
		
		[Serializable] public class Option
		{
			public string name;
			public int weight;
			public string[] data;

			public bool isValid
			{
				get
				{
					return !string.IsNullOrEmpty(name) && (weight > 0); // must have a name & weight
				} 
			}
		}
		
		[Serializable] public class Case
		{
			public string id;
			public string focus;
			public string condition;
			public float percentUser;
			
			public List<Option> options;
			
			public Option FindOption(string optionName)
			{
				return options.FirstOrDefault(op => op.name == optionName);
			}

			public bool isFocusing
			{
				get { return !string.IsNullOrEmpty(focus); }
			}

			public bool acceptingNewUsers
			{
				get { return	!string.IsNullOrEmpty(id) && // has a valid Id
								!isFocusing && // not focusing
								(percentUser > 0) && // has a percentage of users allocated to
								options.Count > 1; // at least 2 options
					
					// check weight per options?
				}
			}
		}
        [Serializable] internal class UserData
        {
            public string m_caseId;
            public string m_optionId;

			[NonSerialized] private UserDataStatus m_status = UserDataStatus.NotLoaded;

			public void Load()
			{
				if (!ValidateStatus(UserDataStatus.NotLoaded, "UserData Loaded before!"))
				{
					return;
				}

				m_status = UserDataStatus.NoData;
				m_caseId = string.Empty;
				m_optionId = string.Empty;
				
				string json = PlayerPrefs.GetString(PPKEY_DATA, string.Empty);
				if (string.IsNullOrEmpty(json)) return;
				
				JsonUtility.FromJsonOverwrite(json, this);
				
				if (!HasCaseAndOptionId)
				{
					m_caseId 	= string.Empty;
					m_optionId 	= string.Empty;
                    Debug.Log("ser case empty");
					return;
				}

				m_status = UserDataStatus.DataLoaded;
			}

			public void Clear()
			{
				m_status = UserDataStatus.NoData;
				m_caseId = string.Empty;
				m_optionId = string.Empty;
				PlayerPrefs.DeleteKey(PPKEY_DATA);
			}

			public void Set(string caseId, string optionId)
			{
				if (string.IsNullOrEmpty(caseId))
				{
					Debug.LogWarning("Set error - caseId & optionId should not be null or emtpy!");
					return;
				}
				
				#if UNITY_EDITOR
				Debug.Log("Set: " + caseId + " --> " + optionId);
				#endif
				
				m_status = UserDataStatus.DataValidated;
				m_caseId = caseId;
				m_optionId = optionId;
				PlayerPrefs.SetString(PPKEY_DATA, JsonUtility.ToJson(this));

                //Analytics.LogEvent(optionId); 
            }
			
			public bool HavingActiveCase()
			{
				return m_status == UserDataStatus.DataValidated;
			}
			
			private bool ValidateStatus(UserDataStatus s, string errorMessage)
			{
				if (m_status == s) return true;
				Debug.LogWarning(errorMessage);
				return false;
			}

			public bool HasCaseAndOptionId
			{
				get { return !string.IsNullOrEmpty(m_caseId) && !string.IsNullOrEmpty(m_optionId); }
			}

			public void ConfirmCaseAndOption(bool isActive) // Active: running, pct user = 0
			{
				if (m_status == UserDataStatus.NotLoaded) Load();
				m_status = isActive ? UserDataStatus.DataValidated : UserDataStatus.DataInvalid;
			}
		}
        internal class AbData
        {
            public List<Case> lstData = new List<Case>();
        }
		internal class CaseManager
		{
			internal Dictionary<string, Case> caseMap;

			public CaseManager(string sjson)
			{
                if(sjson.StartsWith("---", StringComparison.Ordinal))
                {
                    //old sjon value
                    Debug.Log("sjon system");
                    Dictionary<string, string> dict = SuperJson.Partition(sjson);
                    caseMap = new Dictionary<string, Case>();
                    foreach (var kvp in dict)
                    {
                        Case c;

                        try
                        {
                            c = JsonUtility.FromJson<Case>(kvp.Value);
                            c.id = kvp.Key.Trim().ToLower();
                            caseMap.Add(kvp.Key, c);
                        }
                        catch (Exception e)
                        {
                            Debug.LogWarning("Parse failed: " + kvp.Key + "\n" + kvp.Value + " \n" + e);
                        }
                    }
                }
                else
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("{ \"lstData\":");
                    stringBuilder.Append(sjson);
                    stringBuilder.Append("}");
                    var abData = new AbData();
                    try
                    {
                        JsonUtility.FromJsonOverwrite(stringBuilder.ToString(), abData);
                    }
                    catch (Exception ex)
                    {
                        Debug.LogError("Parse json error: " + stringBuilder.ToString() + "\n" + ex.Message);
                    }


                    caseMap = new Dictionary<string, Case>();
                    foreach (var item in abData.lstData)
                    {
                        if (caseMap.ContainsKey(item.id))
                        {
                            Debug.LogWarning("Duplicated AbTest Id: " + item.id);
                            continue;
                        }
                        caseMap.Add(item.id, item);
                    }
                }
                
                Debug.Log("Abtest count : " + caseMap.Count);



    
			}

			public Case TryGetCaseById(string id)
			{
				Case result;
				return caseMap.TryGetValue(id, out result) ? result : null;
			}

			public void RandomSelect(UserData user)
			{
                Debug.Log("Random data");
				Case c = null;
				Option op = null;
				
				var list = new List<Case>();
				
				// filter / remove focus / invalid cases, case with percentage user = 0, other conditions if any
				foreach (var item in caseMap)
				{
					c = item.Value;
					if (!c.acceptingNewUsers) continue;
					list.Add(c);
				}
				
				// no case available
				if (list.Count == 0)
				{
					#if UNITY_EDITOR
					Debug.LogWarning("No ABTest is accepting users!");
					#endif
					
					user.Clear();
					return;
				}
				
				// pick a random case
				int idx = Utils.RandomByWeight(list, (cc) => cc.percentUser, false);
				if (idx == -1)
				{
					//
					user.Clear();
					return;
				}
				
				
				c = list[idx];
				
				// validate available options
				var opList = new List<Option>();
				for (var i = 0; i < c.options.Count; i++)
				{
					op = c.options[i];
					if (!op.isValid) continue;
					opList.Add(op);
				}
				
				// no valid options
				if (opList.Count == 0)
				{
#if UNITY_EDITOR
					Debug.LogWarning("No Option valid for ABTest: " + c.id);
#endif
					user.Clear();
					return;
				}
				
				// pick a random option
				int opIndex = Utils.RandomByWeight(opList, (o) => o.weight, true);
				op = opList[opIndex];

				// Apply selected case / option
				user.Set(c.id, op.name);
			}

			private Dictionary<string, string> _cacheFocusData;
			private Dictionary<string, string> GetFocusABData()
			{
				if (_cacheFocusData != null)
				{
					return Utils.Clone(_cacheFocusData);
				}

				_cacheFocusData = new Dictionary<string, string>();
					
				// merge data of all focused AB
				foreach (var kvp in caseMap)
				{
					Case c = kvp.Value; // case
					if (!c.isFocusing) continue;
					
					Option op = c.FindOption(c.focus);
					Utils.ParseAndAppendData(op.data, _cacheFocusData, false);
				}
				
				return Utils.Clone(_cacheFocusData);
			}

			public Dictionary<string, string> GetABData(UserData user)
			{
				var result = GetFocusABData();
				if (!user.HavingActiveCase()) return result;
				
				Case c = caseMap[user.m_caseId];
				Option op = c.FindOption(user.m_optionId);
				return Utils.ParseAndAppendData(op.data, result, true); // data mod in this option will overwrite focus data
			}
		}

		[SerializeField] private UserData _user;
		[SerializeField] private CaseManager _manager;

        private bool isInited = false;

		#if UNITY_EDITOR
		[SerializeField]
		private List<Case> _listCases;
		#endif

		public void Init(string sjson, int session)
		{
            isInited = true;
            _user = new UserData();
            _user.Load();
			_manager = new CaseManager(sjson);
			
			#if UNITY_EDITOR
			_listCases = _manager.caseMap.Values.ToList();
			#endif

			if (_user.HasCaseAndOptionId)
			{
				var isActive = false;

				Case c = _manager.TryGetCaseById(_user.m_caseId);
				if (c != null && !c.isFocusing) // has valid CaseId & not focusing
				{
					var op = c.FindOption(_user.m_optionId);
					isActive = op != null;
				}

				_user.ConfirmCaseAndOption(isActive);
			}
			else if (session == 1)
			{
				_manager.RandomSelect(_user);
			}

            if(_user != null && !string.IsNullOrEmpty(_user.m_caseId) && !string.IsNullOrEmpty(_user.m_optionId))
            {
                // Cheat to fix firebase set user property 36 character
                // Notes don't choose ab test name too long
                var abProperty = _user.m_caseId + "_" + _user.m_optionId;
                if (abProperty.Length > 36)
                {
                    abProperty = abProperty.Substring(0, 36);
                }
                
                //Analytics.LogAb(abProperty);
            }
            ApplyABData();
		}

		public void Randomize()
		{
			_manager.RandomSelect(_user);
		}
		
		
		// ------------- Asset support -----------------
		
		public ConfigAsset globalConfig;
		void ApplyABData()
		{
			if (globalConfig == null)
			{
				Debug.LogWarning("Apply skipped - list Asset is null or empty!");
				return;
			}

			var dict = _manager.GetABData(_user);
			if (dict == null || dict.Count == 0)
			{
				Debug.LogWarning("Apply skipped - no data to override!");
				return;
			}
            var assetDict = globalConfig.DictConfig;
            foreach(var item in dict)
            {
                var vname = item.Key;
                Asset asset = null;
                if (!assetDict.TryGetValue(vname, out asset))
                {
                    vname = Asset.StringToVariableName(vname);
                    if(!assetDict.TryGetValue(vname, out asset))
                    {

                        LogWarning("Asset: " + vname + " do not have config --> Skip");
                        continue;
                    }
                }
                // valid
#if UNITY_EDITOR
                Debug.Log("Override ab config: " + vname + " ---> " + dict[vname]);
#endif
                asset.FromString(dict[vname]);

            }
		}

		internal static class Utils 
		{
			internal static int RandomByWeight<T>(List<T> list, Func<T, float> wFunc, bool allowScaleUp)
			{
				var n = list.Count;
				if (n == 0) return -1;
				if (n == 1) return 0;
				
				var wList = new List<float>();
				
				// calculate weight & option partition
				var wTotal = 0f;
				for (var i = 0; i < list.Count; i++)
				{
					var w = wFunc(list[i]);
					if (w < 0) Debug.LogWarning("Invalid weight at: " + i + " --> weight = " + w);
					
					wList.Add(wTotal);
					wTotal += w;
				}
				
				// Find the correct segment (partition) based on randomized value
				float rnd = 0;
				if (!allowScaleUp && wTotal < 100)
				{
					rnd = Random.Range(0, 100);
					if (rnd >= wTotal) return -1;
				}
				else
				{
					rnd = Random.Range(0, wTotal);
				}
				
				for (var i = 0; i < wList.Count; i++)
				{
					var w = wList[i];
					if (rnd < w) return i - 1;
				}
				
				return list.Count - 1;
			}
			
			internal static void ParseDataField(string data, out string key, out string value)
			{
				key = null;
				value = null;

				if (string.IsNullOrEmpty(data))
				{
					Debug.LogWarning("ParseDataField() error - Data should not be null or empty!");
					return;
				}

				var idx = data.IndexOf(':');
				if (idx == -1)
				{
					Debug.LogWarning(
						"ParseDataField() error - Data is not in correct format, must be in the form of [key:value]");
					return;
				}

				key = data.Substring(0, idx).Trim();
				idx++;
				value = data.Substring(idx, data.Length - idx).Trim();
			}

			internal static Dictionary<string, string> Clone(Dictionary<string, string> source)
			{
				var d = new Dictionary<string, string>();
				foreach (var item in source)
				{
					d.Add(item.Key, item.Value);
				}

				return d;
			}

			internal static Dictionary<string, string> ParseAndAppendData(string[] data,
				Dictionary<string, string> dict, bool overwrite = false)
			{
				if (dict == null)
				{
					dict = new Dictionary<string, string>();
				}

				for (var i = 0; i < data.Length; i++)
				{
					string k;
					string v;
					ParseDataField(data[i], out k, out v);
					if (string.IsNullOrEmpty(k)) continue;

					if (dict.ContainsKey(k))
					{
						Debug.LogWarning("Multiple item with the same key found in data..." + k);
						if (overwrite) dict[k] = v;
					}
					else
					{
						dict.Add(k, v);
					}
				}

				return dict;
			}
		}
        private void LogWarning(string log)
        {
#if ENABLE_LOG
            Debug.LogWarning(log);
#endif
        }
    }	
}