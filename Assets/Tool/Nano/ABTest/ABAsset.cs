﻿using System.Collections;
using System.Collections.Generic;
using T70.VariableAsset;
using UnityEngine;

namespace nano
{
    [CreateAssetMenu(fileName = "AbManagerAsset", menuName = "Variable Asset/AbManager")]
    public class ABAsset : AssetT<AB>
    {
        public IntAsset sessionAsset;
        public override void FromJson(string json)
        {
            // Fix ArgumentException: JSON parse error: Invalid value
            // https://developer.cloud.unity3d.com/diagnostics/orgs/team70/projects/d62d5a15-9012-418e-a408-140af1e3276e/crashes/22c89c3fb61dac1f5f5037e6fec6a9df/
            if (string.IsNullOrEmpty(json))
            {
                Debug.LogWarning("Invalid json - json should not be null or empty!");
                return;
            }
            Value.Init(json, sessionAsset.Value);
        }
    }

}
