﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;
namespace nano
{
    [CustomEditor(typeof(MeshFilter), true)]
    [CanEditMultipleObjects()]
    public class VertexColorCreaterEditor : Editor
    {
        private string MaterialGUID = "a166912fddaca4759b8875e096983798";//Default Vertex Color material
        private Material material;
        private bool isOptimized = false;
        private bool checkOptimized;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            return;
            if (material == null)
            {
                material = AssetDatabase.LoadAssetAtPath<Material>(AssetDatabase.GUIDToAssetPath(MaterialGUID));
            }
            material = (Material)EditorGUILayout.ObjectField("Default Material Vertex", material, typeof(Material), false);


            if (GUILayout.Button("Split mesh"))
            {

                foreach(var item in targets)
                {
                    MeshFilter filter = item as MeshFilter;
                    SplitMesh(filter);
                }

            }

            if(!checkOptimized)
            {
                checkOptimized = true;
                isOptimized = (target as MeshFilter).GetComponent<VertexColorMono>() != null;
            }
            if (isOptimized)
            {
                return;
            }

            if (GUILayout.Button("Convert To VertexColor"))
            {
                foreach (var item in targets)
                {
                    MeshFilter filter = item as MeshFilter;
                    GenerateObject(filter);
                }



            }


            if (GUILayout.Button("Optimize"))
            {
                foreach (var item in targets)
                {
                    MeshFilter filter = item as MeshFilter;
                    GenerateOptimizeObject(filter);
                }



            }
        }


        private void GenerateObject(MeshFilter meshSource)
        {
            var sharedMesh = meshSource.sharedMesh;
            var subMeshCount = sharedMesh.subMeshCount;
            var sourceMaterials = meshSource.GetComponent<MeshRenderer>().sharedMaterials;

            //clone mesh
            var mesh = new Mesh();
            mesh.vertices = sharedMesh.vertices;
            mesh.triangles = sharedMesh.triangles;
            mesh.RecalculateBounds();//...
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();

            //save mesh
            //string path = EditorUtility.SaveFilePanel("Save Separate Mesh Asset", "Assets/", name, "asset");
            //path = "Assets" + path.Replace(Application.dataPath, string.Empty);
            string path = AssetDatabase.GetAssetPath(sharedMesh);
            path = path.Substring(0, path.LastIndexOf('.'));

            //// Duplicate mesh with difference color
            Vector3 colorID = Vector3.zero;
            for (int i = 0; i < sourceMaterials.Length; i++)
            {
                var c = sourceMaterials[i].color;
                colorID += new Vector3(c.r, c.g, c.b);
            }
            /// 

            path = string.Format( "{0} [Mesh_Vertex_Color]_{1}.asset", path, colorID);

            var savedMesh = AssetDatabase.LoadAssetAtPath<Mesh>(path);
            if (savedMesh == null)
            {
                AssetDatabase.CreateAsset(mesh, path);
                AssetDatabase.SaveAssets();
                savedMesh = AssetDatabase.LoadAssetAtPath<Mesh>(path);//load again
            }
            


            //get asset
            //create child
            var vertexMono = meshSource.gameObject.AddComponent<VertexColorMono>();

            //set color


            List<SubmeshData> colorVertexIndex = new List<SubmeshData>();
            for (int i = 0; i < subMeshCount; i++)
            {
                var submeshData = new SubmeshData();
                submeshData.color = sourceMaterials[i].color;
                var list = new List<int>();
                var subMesh = sharedMesh.GetTriangles(i);
                
                for (int k = 0; k < subMesh.Length; k++)
                {
                    list.Add(subMesh[k]);
                }

                submeshData.listVertiecsIndex = list.ToArray();
                colorVertexIndex.Add(submeshData);
            }
            vertexMono.colorVertexIndex = colorVertexIndex.ToArray();
            vertexMono.sourceMaterials = sourceMaterials;
            vertexMono.sourceMesh = sharedMesh;

            vertexMono.vertexMaterial = material;
            vertexMono.VertexMesh = savedMesh;
            vertexMono.allowValidate = true;
            vertexMono.UseVertexColor();
        }


        //
        private void GenerateOptimizeObject(MeshFilter meshSource)
        {
            var sharedMesh = meshSource.sharedMesh;
            var subMeshCount = sharedMesh.subMeshCount;
            var sourceMaterials = meshSource.GetComponent<MeshRenderer>().sharedMaterials;

            var normals = sharedMesh.normals;
            var triangles = sharedMesh.triangles;
            var vertices = sharedMesh.vertices;
            List<int> lstTriads = new List<int>();
            var go1 = new GameObject("n");
            var tf1 = go1.transform;
            HashSet<Vector3> pos = new HashSet<Vector3>();
            for (int i = 0; i < normals.Length; i++)
            {
                var faceNormal = (normals[triangles[i + 0]] + normals[triangles[i + 1]] + normals[triangles[i + 2]]) / 3.0f;
                Debug.Log(faceNormal);
                if(!pos.Contains(faceNormal))
                {
                    var g = new GameObject(faceNormal.ToString());
                    g.transform.localPosition = faceNormal;
                    g.transform.SetParent(tf1);
                    pos.Add(faceNormal);
                    Debug.DrawLine(tf1.transform.position, faceNormal, Color.green, 1000);
                }

                if (((int)Mathf.Abs(faceNormal.x)  == 1) || ((int)Mathf.Abs(faceNormal.y) == 1) || ((int)Mathf.Abs(faceNormal.z) == 1))
                {
                    lstTriads.Add(triangles[i + 0]);
                    lstTriads.Add(triangles[i + 1]);
                    lstTriads.Add(triangles[i + 2]);
                }

            }
            //var nMesh = GetSubMesh(vertices, lstTriads.ToArray());
            //var go = new GameObject("n");
            //var ren = go.AddComponent<MeshRenderer>();
            //var filter = go.AddComponent<MeshFilter>();

            //filter.sharedMesh = nMesh;

            //return;

            ////clone mesh
            //var mesh = new Mesh();
            //mesh.vertices = sharedMesh.vertices;
            //mesh.triangles = sharedMesh.triangles;
            //mesh.RecalculateBounds();//...
            //mesh.RecalculateNormals();
            //mesh.RecalculateTangents();

            ////save mesh
            ////string path = EditorUtility.SaveFilePanel("Save Separate Mesh Asset", "Assets/", name, "asset");
            ////path = "Assets" + path.Replace(Application.dataPath, string.Empty);
            //string path = AssetDatabase.GetAssetPath(sharedMesh);
            //path = path.Substring(0, path.LastIndexOf('.'));

            ////// Duplicate mesh with difference color
            //Vector3 colorID = Vector3.zero;
            //for (int i = 0; i < sourceMaterials.Length; i++)
            //{
            //    var c = sourceMaterials[i].color;
            //    colorID += new Vector3(c.r, c.g, c.b);
            //}
            ///// 

            //path = string.Format("{0} [Mesh_Vertex_Color]_{1}.asset", path, colorID);

            //var savedMesh = AssetDatabase.LoadAssetAtPath<Mesh>(path);
            //if (savedMesh == null)
            //{
            //    AssetDatabase.CreateAsset(mesh, path);
            //    AssetDatabase.SaveAssets();
            //    savedMesh = AssetDatabase.LoadAssetAtPath<Mesh>(path);//load again
            //}



            ////get asset
            ////create child
            //var vertexMono = meshSource.gameObject.AddComponent<VertexColorMono>();

            ////set color


            //List<SubmeshData> colorVertexIndex = new List<SubmeshData>();
            //for (int i = 0; i < subMeshCount; i++)
            //{
            //    var submeshData = new SubmeshData();
            //    submeshData.color = sourceMaterials[i].color;

            //    submeshData.listVertiecsIndex = new List<int>();
            //    var subMesh = sharedMesh.GetTriangles(i);
            //    for (int k = 0; k < subMesh.Length; k++)
            //    {
            //        submeshData.listVertiecsIndex.Add(subMesh[k]);
            //    }
            //    colorVertexIndex.Add(submeshData);
            //}
            //vertexMono.colorVertexIndex = colorVertexIndex;
            //vertexMono.sourceMaterials = sourceMaterials;
            //vertexMono.sourceMesh = sharedMesh;

            //vertexMono.vertexMaterial = material;
            //vertexMono.VertexMesh = savedMesh;
            //vertexMono.allowValidate = true;
            //vertexMono.UseVertexColor();
        }


        private Mesh GetSubMesh(Vector3[] vertices, int[] triads)
        {
            var dict = new Dictionary<int, int>(); // vertexIdx --> newVertexIdx

            var list = new List<Vector3>();
            var newTriads = new int[triads.Length];

            for (int i = 0; i < triads.Length; i++)
            {
                var vertexIdx = triads[i];
                var newVertexIdx = 0;

                if (dict.TryGetValue(vertexIdx, out newVertexIdx))
                {
                    newTriads[i] = newVertexIdx;
                    continue;
                }

                newVertexIdx = dict.Count;
                newTriads[i] = newVertexIdx;

                dict.Add(vertexIdx, newVertexIdx);
                list.Add(vertices[vertexIdx]);

            }

            var m = new Mesh();
            m.vertices = list.ToArray();
            m.triangles = newTriads;
            m.RecalculateNormals();

            return m;
        }

        private void SplitMesh(MeshFilter meshSource)
        {
            var sharedMesh = meshSource.sharedMesh;
            var subMeshCount = sharedMesh.subMeshCount;

            var vertices = sharedMesh.vertices;
            var sourceMaterials = meshSource.GetComponent<MeshRenderer>().sharedMaterials;

            string path = AssetDatabase.GetAssetPath(sharedMesh);
            path = path.Substring(0, path.LastIndexOf('.'));
            path += " [Mesh_Vertex_Color]";
            var subMeshPath = string.Format("{0}/{1}_submesh_", path, sharedMesh.name);
            Debug.Log("subMeshPath: " + subMeshPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                for (int i = 0; i < subMeshCount; i++)
                {
                    var triads = sharedMesh.GetTriangles(i);
                    var mesh = GetSubMesh(vertices, triads);
                    var p = string.Format("{0}{1}.asset", subMeshPath, i);
                    AssetDatabase.CreateAsset(mesh, p);

                }
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

            }
            for (int i = 0; i < subMeshCount; i++)
            {
                var p = string.Format("{0}{1}.asset", subMeshPath, i);
                var savedMesh = AssetDatabase.LoadAssetAtPath<Mesh>(p);

                var go = new GameObject("SubMesh_" + i);
                go.transform.SetParent(meshSource.transform);
                go.transform.localScale = Vector3.one;
                go.transform.localPosition = Vector3.zero;
                go.transform.localRotation = Quaternion.identity;

                go.AddComponent<MeshFilter>();
                Debug.Log("sa: " + (savedMesh == null) + "  " + AssetDatabase.GetAssetPath(savedMesh));

                var render = go.AddComponent<MeshRenderer>();
                render.sharedMaterial = material;

                var VL = go.AddComponent<VLVariation>();
                VL.filter = go.AddComponent<MeshFilter>();
                VL.mesh = savedMesh;
                //VL.meshId = savedMesh.name + ;
                VL.color = sourceMaterials[i].color;
                VL.meshId = VL.color.ToString();

                //VL.ReloadMesh();
                VL.OnValidate();
            }
            meshSource.GetComponent<MeshRenderer>().enabled = false;



        }
    }
}
#endif