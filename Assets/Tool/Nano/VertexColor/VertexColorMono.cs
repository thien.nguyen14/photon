﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace nano
{
    [System.Serializable]
    public class SubmeshData
    {
        public Color color;
        public int[] listVertiecsIndex;
    }
    [RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
    public class VertexColorMono : MonoBehaviour
    {
        public SubmeshData[] colorVertexIndex;
        private MeshFilter meshFilter;
        private MeshRenderer meshRenderer;

        private Color[] colors;

        private void Awake()
        {
            if (meshFilter == null) meshFilter = GetComponent<MeshFilter>();
            if (meshRenderer == null) meshRenderer = GetComponent<MeshRenderer>();
            _mesh = meshFilter.sharedMesh;
            _colorVertexIndexCount = colorVertexIndex.Length;
        }

        private Mesh _mesh;
        private int _colorVertexIndexCount;

        public void RefreshColor()
        {
            if (_mesh == null) return;

            if (colors == null) colors = new Color[_mesh.vertices.Length];

            for (int i = 0; i < colorVertexIndex.Length; i++)
            {
                var subMesh = colorVertexIndex[i];
                var arrLst = subMesh.listVertiecsIndex;
                var lstCount = arrLst.Length;
                for (int k = 0; k < lstCount; k++)
                {
                    colors[arrLst[k]] = subMesh.color;
                }
            }
            _mesh.colors = colors;
        }


#if UNITY_EDITOR
        [Header("Editor Value")]
        public bool allowValidate = false;
        public Mesh sourceMesh;
        public Material[] sourceMaterials;

        public Mesh VertexMesh;
        public Material vertexMaterial;

        public void UseVertexColor()
        {
            var meshParent = GetComponent<MeshFilter>();
            var render = meshParent.GetComponent<MeshRenderer>();


            meshParent.sharedMesh = VertexMesh;
            render.sharedMaterials = new Material[0];
            render.sharedMaterial = vertexMaterial;
            RefreshColor();
        }
        public void RevertSource()
        {

            var meshParent = GetComponent<MeshFilter>();
            var render = meshParent.GetComponent<MeshRenderer>();
            meshParent.sharedMesh = sourceMesh;
            render.sharedMaterials = sourceMaterials;
        }

        // private void OnValidate()
        // {
		// 	Debug.Log("ON VALIDATE!");

        //     if (!allowValidate) return;
        //     RefreshColor();
        // }
#endif



    }

#if UNITY_EDITOR
    [CustomEditor(typeof(VertexColorMono))]
    public class VertexColorMonoEditor:Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var source = target as VertexColorMono;
            var mesh = source.GetComponent<MeshRenderer>();
            if (mesh == null) return;

            if(mesh.sharedMaterial == source.vertexMaterial)
            {
                //revert
                if(GUILayout.Button("Revert Parent"))
                {
                    source.RevertSource();
                }
            }
            else
            {
                if (GUILayout.Button("Release Parent"))
                {
                    source.UseVertexColor();
                }
            }
        }
    }
#endif


}
