﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class VLVariation : MonoBehaviour
{
    public string meshId;
    public MeshFilter filter;
    public Color color;
    
    
    public Mesh mesh;

    void OnEnable()
    {
        if (filter == null) return;
        if (mesh == null) return;

        Refresh();
    }

    void Awake()
    {
        Refresh();
    }
    
    #if UNITY_EDITOR
    [SerializeField] private Color lastColor;
    public void OnValidate()
    {
        if (Application.isPlaying) return;
        
        if (filter == null)
        {
            filter = GetComponent<MeshFilter>();
            if (filter == null) return;
        }

        if (filter.sharedMesh != mesh)
        {
            // right after awake?
            if (filter.sharedMesh == null)
            {
                if (mesh == null) return;
                DelayRefresh();
                return;
            }
            
            // right after awake?
            if (filter.sharedMesh.hideFlags != HideFlags.DontSave) // this is NOT a cloned mesh
            {
                // mesh source changed
                mesh = filter.sharedMesh;
                DelayRefresh();
                return;
            }
        }
        
        if (string.IsNullOrEmpty(meshId)) return;
        if (mesh == null) return;
        
        if (color != lastColor)
        {
            lastColor = color;
            DelayRefresh();
        }
    }

    void DelayRefresh()
    {
        EditorApplication.update -= Refresh;
        EditorApplication.update += Refresh;
    }

    #endif

    void Refresh()
    {
        #if UNITY_EDITOR
        EditorApplication.update -= Refresh;
        #endif

        if (filter == null) return;
        if (mesh == null) return;
        if (string.IsNullOrEmpty(meshId)) return;
        Debug.Log("Refresh color");
        filter.sharedMesh = VLMesh.Get(mesh, meshId, color);
    }
}
