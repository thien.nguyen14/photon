﻿using System.Collections.Generic;
using UnityEngine;

public class VLMesh
{
	[System.Serializable] public class VInfo
	{
		public string id;
		public Mesh mesh;
		public Color color;

		private Color[] vColors;
		
		public Mesh ApplyColor(Color? newColor = null, bool clearVColors = false)
		{
			if (newColor != null)
			{
				color = newColor.Value;
			}
			
			// update vertices in mesh
			if (vColors == null || vColors.Length != mesh.vertexCount)
			{
				vColors = new Color[mesh.vertexCount];
			}
			
			// Do apply color
			for (var i = 0;i < vColors.Length; i++)
			{
				vColors[i] = color;
			}
			
			mesh.colors = vColors;
			if (clearVColors) vColors = null;
			return mesh;
		}
	}

	[System.Serializable] public class MInfo
	{
		public Mesh mesh;
		public Dictionary<string, VInfo> vMap = new Dictionary<string, VInfo>();
		
		#if UNITY_EDITOR
		public List<VInfo> vList = new List<VInfo>(); // for debugging purpose
		#endif

		public Mesh GetVariation(string id, bool autoNew = true, Color? newColor = null, bool clearVColors = false)
		{
			VInfo v;
			
			if (!vMap.TryGetValue(id, out v))
			{
				if (!autoNew) return null;
				if (newColor == null) newColor = Color.white; //default color: White

				v = new VInfo()
				{
					id = id,
					mesh = new Mesh
					{
						hideFlags = HideFlags.DontSave,
						vertices = mesh.vertices,
						triangles = mesh.triangles,
						uv = mesh.uv,
						normals = mesh.normals,
						tangents = mesh.tangents
					}
				};
				
				vMap.Add(id, v);
				
				#if UNITY_EDITOR
				vList.Add(v);
				#endif
			}
			
			if (newColor != null) v.ApplyColor(newColor, clearVColors);
			return v.mesh;
		}
	}

	private static Dictionary<Mesh, MInfo> meshMap = new Dictionary<Mesh, MInfo>();

	public static Mesh Get(Mesh mesh, string id, Color? c = null)
	{
		if (mesh == null)
		{
			Debug.LogWarning("Mesh should not be null!");
			return mesh;
		}

		#if UNITY_EDITOR
		if (mesh.hideFlags == HideFlags.DontSave)
		{
			Debug.LogWarning("[Editor] You are trying to clone a non-persistent mesh - which might cause memory leaks!");
		}
		#endif
		
		MInfo mInfo;
		if (!meshMap.TryGetValue(mesh, out mInfo))
		{
			mInfo = new MInfo()
			{
				mesh = mesh
			};
			meshMap.Add(mesh, mInfo);
		}
		
		return mInfo.GetVariation(id, true, c);
	}
}





