﻿using System.Collections;
using System.Collections.Generic;
using T70.VariableAsset;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "GlobalVA", menuName = "Assets/Global VA")]
public class GlobalVA : ScriptableObject
{
    private static GlobalVA instance;
    
    public void Init()
    {
        if (instance != null)
        {
            Debug.LogWarning("Multiple instance found for GlobalVA");
            return; // ignore !
        }
        
        instance = this;
    }

    public static bool IsInit => instance != null;
    
    [SerializeField] internal GameStateAsset _gameState;
    public static GameStateAsset gameStateVA
    {
        get { return instance._gameState; }
    }

    [SerializeField] internal BackButtonAsset _backButton;
    public static BackButtonAsset backButton
    {
        get { return instance._backButton; }
    } 
}
