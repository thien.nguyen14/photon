﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace T70.VariableAsset
{

    public enum GameState
    {  
		Menu,		// Menu-UI
		LoadSong,	// Loading-UI
        
		
        Start,		// Play song
        First,      // Hit the first note
        Play,       // Controllable now
        OutOfPlatform,		// Controllable now
		Die,		// Hit the wrong tiles
		Outtro,		// Play End game OutTro
        Continue,   // Begin Continue
        Tutorial,   // Begin Continue
        Hold_Tutorial,
        FirstTime_Tutorial,



        WaitForRevive,
		Revive,		// Back to play count down - continue popup
        End,			// End-UI,
        Shop,
        Achievement,
        DiscoveryCatalog,
        DiscoveryDetailArtist,
        DiscoveryDetailGenres,
        DiscoveryDetailSong,
        DiscoverySearch,
        Intro,
        VipSingle,
        None, 		// GamePlay-UI
        Splash		// Splash-UI
    }


	[CreateAssetMenu(fileName = "GameState Asset", menuName = VariableConst.MenuPath.GameState, order = VariableConst.Order.GameState)]
	public class GameStateAsset : AssetT<GameState> 
	{
        private Stack<GameState> stateStack = new Stack<GameState>();
        public override GameState Value
        {
            get
            {
                return base.Value;
            }
            set
            {
	            if (value == GameState.Menu)
	            {
		            stateStack.Clear();
	            }
	            else
	            {
		            stateStack.Push(base.Value);
	            }
                
                //Debug.Log("Add state: " + base.Value + "  to stack");
                base.Value = value;



            }
        }

        public void BackToPreviousState()
        {
            //Debug.Log("back to previous state");
            if (stateStack.Count == 0) return;
            var state = stateStack.Pop();
            base.Value = state;
        }






#if UNITY_EDITOR
        public override void OnDraw(Rect rect) 
		{
			EditorGUI.BeginChangeCheck();
			{
				Value = (GameState)EditorGUILayout.EnumPopup(Value);
			}
			if (EditorGUI.EndChangeCheck()) 
			{
				EditorUtility.SetDirty(this);
			}
		}
		#endif
	}
}