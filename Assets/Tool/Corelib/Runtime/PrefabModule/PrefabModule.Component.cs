#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
namespace com.team70
{
	public partial class PrefabModule
	{
		private const string EXPORT_PATH = "Assets/PrefabModule/";
		public static readonly List<string> AvailableComponentID = new List<string>();
		
		
		public static void RefreshAvailableComponentIDs()
		{
			if (!Directory.Exists(EXPORT_PATH)) return;
			
			AvailableComponentID.Clear();
			var files = Directory.GetFiles(EXPORT_PATH);
			foreach (var f in files)
			{
				if (!f.EndsWith(".json")) continue;
				var cid = f.Substring(f.LastIndexOf("/", StringComparison.Ordinal)+1).Replace(".json", string.Empty);
				
				AvailableComponentID.Add(cid);
				Debug.Log(cid);
			}
		}
		
		[Serializable] internal class ComponentInfo
		{
			public string componentId;
			public string fullTypeName; // default fullTypeName
			public List<CEInfo> elements = new List<CEInfo>();
		}

		[Serializable] internal class CEInfo
		{
			public string id;
			public string componentType;
			public string childName;
		}

		public string componentId;
		
		[ContextMenu("Save Component")]
		public void SaveComponent()
		{
			var fullTypeName = script?.classType?.fullTypeName;
			
			if (string.IsNullOrEmpty(componentId))
			{
				componentId = string.IsNullOrEmpty(fullTypeName) 
					? name.Replace(" ", "-") 
					: fullTypeName.Replace(".", "-");
			}
			
			var cInfo = new ComponentInfo
			{
				fullTypeName = fullTypeName,
				componentId = componentId
			};

			for (var i = 0; i < lstElement.Count; i++)
			{
				ElementInfo elm = lstElement[i];
				var childName = string.Empty;

				if (elm.component != null && elm.component.Count > 0)
				{
					var c = elm.component[0];
					if (c != null)
					{
						Transform t = transform;
						Transform p = c.transform.parent;
						var list = new List<string>
						{
							c.name
						};

						while (p != null && p != t)
						{
							list.Add(p.name);	
							p = p.parent;
						}

						if (p == null)
						{
							childName = string.Empty;
							Debug.LogWarning($"Invalid component: must be child of PrefabModule: {c}");
						}
						else
						{
							list.Reverse();
							childName = string.Join("/", list.ToArray());
						}
					}
				}
				
				cInfo.elements.Add(new CEInfo()
				{
					id = elm.id,
					componentType =  elm.componentType,
					childName = childName
				});
			}
			
			var data = JsonUtility.ToJson(cInfo, true);
			
			Debug.LogWarning(data);
			Directory.CreateDirectory(EXPORT_PATH);
			File.WriteAllText($"{EXPORT_PATH}{componentId}.json", data);
			
			AssetDatabase.Refresh();
		}
		
		public void LoadComponent(string cid)
		{
			if (string.IsNullOrEmpty(cid))
			{
				Debug.LogWarning("Invalid Component Id");
				return;
			}

			var file = $"{EXPORT_PATH}{cid}.json";
			if (!File.Exists(file))
			{
				Debug.LogWarning("Component ID not existed!");
				return;
			}
			
			var json = File.ReadAllText(file);
			var cInfo = JsonUtility.FromJson<ComponentInfo>(json);
			if (cInfo == null)
			{
				Debug.LogWarning($"Invalid file format: {file}\n{json}");
				return;
			}
			
			Undo.RecordObject(this, $"Load Component: {cid}");
			lstElement.Clear();
			
			componentId = cInfo.componentId;
			if (!string.IsNullOrEmpty(cInfo.fullTypeName))
			{
				SerializableType classType =script?.classType;
				if (classType != null)
				{
					classType.fullTypeName = cInfo.fullTypeName;
					classType.RefreshCacheType();	
				}
			}
			
			for (var i = 0; i < cInfo.elements.Count; i++)
			{
				CEInfo e = cInfo.elements[i];
				Component c = GetChildByName(transform, e.childName, e.componentType);
				
				lstElement.Add(new ElementInfo()
				{
					id = e.id, 
					componentType = e.componentType,
					component = new List<Component>{ c }
				});
			}
			
			EditorUtility.SetDirty(this);
		}
		
		static Component GetChildByName(Transform t, string childName, string componentType)
		{
			var arr = childName.Split('/');
			Transform c = t;
			
			for (var i = 0; i < arr.Length; i++)
			{
				c = c.Find(arr[i]);
				if (c == null) return null;
			}
			
			return c.gameObject.GetComponent(componentType);
		}
	}
}
#endif
