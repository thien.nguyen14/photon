﻿#if UNITY_EDITOR
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;



namespace vietlabs.vc2t
{
	public class DiffuseColor2Texture
	{
		[MenuItem("T70/Tools/Scale Diffuse Color UV")]
		public static void ScaleUV()
		{
			var s = Utils.GetSelection<MeshFilter>();
			// Debug.Log("S:: => " + s.Count);

			foreach (MeshFilter mf in s)
			{
				Mesh m = mf.sharedMesh;
				var uvs = m.uv;

				for (var i = 0;i < uvs.Length; i++)
				{
					uvs[i] = new Vector2(0.5f, 1 / 255f);
				}
				
				// Debug.Log(m.name + " --> " + uvs.Length);

				var m2 = UnityEngine.Object.Instantiate(m);
				m2.uv = uvs;
				mf.sharedMesh = m2;
			}
		}

		
		[MenuItem("T70/Tools/Material Color -> Texture")]
		public static void GenerateTexture()
		{
			var s = Selection.activeObject;

			if (!(s is Material o)) 
			{
				Debug.LogWarning("Must select material");
				return;
			}

			var c = o.color;

			var tex = (Texture2D) o.mainTexture;
			if (tex != null)
			{
				Debug.LogWarning("Already have a texture!");
				return;
			}

			int w = 4;
			int h = 256;

			tex = new Texture2D(w, h, TextureFormat.RGBA32, false, false);
			for (int i =0 ;i < w; i++)
			{
				for (int j = 0; j < h; j++)
				{
					tex.SetPixel(i, j, c);
				}
			}
			tex.Apply();

			var texPath = AssetDatabase.GetAssetPath(o).Replace(".mat", ".png");
			File.WriteAllBytes(texPath, tex.EncodeToPNG());

			AssetDatabase.ImportAsset(texPath, ImportAssetOptions.ForceSynchronousImport);
			
			var tex2 = AssetDatabase.LoadAssetAtPath<Texture2D>(texPath);
			o.color = Color.white;
			o.mainTexture = tex2;
		}
	}
}

#endif