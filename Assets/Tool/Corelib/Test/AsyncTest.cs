﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using com.team70;
using UnityEngine;

public class AsyncTest : MonoBehaviour
{
    void Start()
    {
        Async.CreateInstance();
    }
    
    [Button] void Test()
    {
        StopAllCoroutines();
        StartCoroutine(DoTest());
    }

    IEnumerator DoTest()
    {
        yield return null;
        
        var time = Time.time;
        
        Debug.LogWarning("Time: " + time);
        
        for (var i = 0; i < 100; i++)
        {
            var id0 = i;
            var idx = 100 - i;
            var dTime = idx * 1/120f;
            
            Async.Call(Abc, dTime);
            
            Async.Call(() =>
            {
                Debug.Log($"Invert Order call {time} : call {idx}");
            }, 0);
        }
    }

    void Abc()
    {
        // Debug.Log($"Ordering call - {time + dTime} : call {id0}");
    }
    
    
    [Button] void ThreadTest()
    {
        for (var i = 0; i < SystemInfo.processorCount; i++)
        {
            var t = new Thread(ThreadUpdate);
            t.Start();
        }
    }

    static void ThreadUpdate()
    {
        const int max = 1000 * 10;
        var counter = 0;
        
        for (var i = 0; i < max; i++)
        {
            var threadID = Thread.CurrentThread.ManagedThreadId;
            Async.Call(() =>
            {
                counter++;
                if (counter == max) Debug.Log($"Thread {threadID}: updateCount = {counter}/{max}");
            }, 0f);
        }
    }
}
