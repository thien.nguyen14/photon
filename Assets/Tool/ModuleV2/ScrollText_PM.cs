﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.team70;
using T70.VariableAsset;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[Serializable] public class ScrollTextItem
{
    public Text m_TitleTxt;
    public RectTransform m_RectTransform;

    public ScrollTextItem(Text text, RectTransform rect)
    {
        m_TitleTxt = text;
        m_RectTransform = rect;
    }

    // public void RefreshText(string text, float size)
    // {
    //     m_TitleTxt.text = text;
    //     m_RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size);
    // }
}

public class ScrollText_PM : UIBehaviour, IPrefabModuleDataLogic<string>, IPrefabModuleLocalize
{
    public const int nItem = 2;
    
    [Header("Elements")] 
    public PrefabModule m_View;
    public Text m_Prefab;
    
    
    [Header("Config")]
    public float speed;
    public float maxSize; // if maxSize is set :: Auto size
    
    [SerializeField] private List<ScrollTextItem> _lstItem;
    [SerializeField] private RectTransform _viewport;
    
    [SerializeField] private Vector2 itemSize;
    [SerializeField] private Vector2 viewportSize;
    [SerializeField] private Vector2 childrenOffsetV;

    [SerializeField] private float _preferredWidthText;
    [SerializeField] private float _currentPositionRecycleView;
    [SerializeField] private Coroutine _moveTextRoutine;
    [SerializeField] private bool _inited;

    private string _currentText;
    private RectMask2D _mask;
    // public bool willLog = false;
    
    private void OnEnable()
    {
        if (_moveTextRoutine != null) StopCoroutine(_moveTextRoutine);
        _moveTextRoutine = StartCoroutine(MoveText());
    }

    private void OnDisable()
    {
        if (_moveTextRoutine != null) StopCoroutine(_moveTextRoutine);
    }
    
    public void Init(PrefabModule module, object initData = null)
    {
        if (_inited) return;
        
        var text = (string) initData;
        m_View = module;
        _mask = m_View.gameObject.AddComponent<RectMask2D>();
        
        speed   = module.GetConfig<float>("speed", 200f);
        maxSize = module.GetConfig<float>("maxSize", 0f);
        
        // Viewport
        _viewport = (RectTransform) m_View.transform;
        InstancePrefab();
        _currentText = string.IsNullOrEmpty(text) ? m_Prefab.text : text;
        _inited = true;
        
        // if (willLog) Debug.Log(this + " Init()");
    }

    IEnumerator MoveText()
    {
        // wait for init
        while (!_inited)
        {
            yield return null;
        }
        
        // Recalculate viewport size
        while (viewportSize.x <= 1) // invalid viewport size - 1px: prevent nominal case when width ~ 1e^-30
        {
            RefreshViewPort();
            if (viewportSize.x > 1) break;
            yield return null;
        }
        
        // Recalculate size (if needed)
        CalculateTextWidth();

        if (AlignText2Viewport() == false)
        {
            _mask.enabled = false;
            yield break;
        }

        _mask.enabled = true;
        while (true)
        {
            yield return null;

            var delta = speed * Time.deltaTime;
            _currentPositionRecycleView += delta;

            for (var i = 0; i < _lstItem.Count; i++)
            {
                var _trans = _lstItem[i].m_RectTransform;
                var v  = _trans.anchoredPosition;
                var x = itemSize.x * i + childrenOffsetV.x;
                _trans.anchoredPosition = new Vector2(x - _currentPositionRecycleView, v.y);
            }
            
            if (_currentPositionRecycleView >= itemSize.x) _currentPositionRecycleView = 0;
        }
    }

    protected override void OnRectTransformDimensionsChange()
    {
        if (!_inited || !enabled || !gameObject.activeInHierarchy) return;

        var viewPortResized = viewportSize.x > 0 && ((int)viewportSize.x != (int)_viewport.rect.size.x);
        if (!viewPortResized) return;
        
        Async.Call(() =>
        {
            if (!enabled || !gameObject.activeInHierarchy) return;
            
            RefreshViewPort();
            if (AlignText2Viewport() == false) return;
        
            // Move text
            if (_moveTextRoutine != null) StopCoroutine(_moveTextRoutine);
            _moveTextRoutine = StartCoroutine(MoveText());
        }, this.GetInstanceID());
    }

    void CalculateTextWidth()
    {
        // text changed?
        var text0 = _lstItem[0].m_TitleTxt;
        text0.text = _currentText;
        _preferredWidthText = text0.preferredWidth;
        
        // Resize viewport if needed
        if (maxSize > 0) ResizeViewport();
        
        // Update text1 (if needed)
        Text text1 = _lstItem[1].m_TitleTxt;
        text1.enabled = _preferredWidthText > viewportSize.x;
        text1.text = _currentText;
        
        // Update children offsetV
        childrenOffsetV.x = text0.rectTransform.pivot.x * _preferredWidthText;
    }
    
    void ResizeViewport()
    {
        var w = Mathf.Min(_preferredWidthText, maxSize);
        viewportSize.x = w;
        _viewport.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
        // if (willLog) Debug.LogWarning("Refresh ViewPort: " + viewportSize);
    }
    
    float GetAlignPercent(TextAnchor ta)
    {
        switch (ta)
        {
            case TextAnchor.LowerLeft:
            case TextAnchor.MiddleLeft:
            case TextAnchor.UpperLeft:
            {
                return 0;
            }
            
            case TextAnchor.LowerRight:
            case TextAnchor.MiddleRight:
            case TextAnchor.UpperRight:
            {
                return 1;
            }
            
            default: return 0.5f;
        }
    }
    
    private bool AlignText2Viewport()
    {
        var width = viewportSize.x;
        itemSize.x = _preferredWidthText > width ? _preferredWidthText + width / 2 : width;
        
        var isMoveText = _preferredWidthText > viewportSize.x;
        var alignPercent = GetAlignPercent(m_Prefab.alignment); // Non-resizable texts will align left
        var ox = (alignPercent * (width - _preferredWidthText)) + childrenOffsetV.x;
        
        for (var i = 0; i < _lstItem.Count; i++)
        {
            if (isMoveText == false && i > 0)
            {
                _lstItem[i].m_TitleTxt.gameObject.SetActive(false);
                continue;
            }

            ScrollTextItem item = _lstItem[i];
            item.m_TitleTxt.gameObject.SetActive(true);
            RectTransform _trans = item.m_RectTransform;
        
            Vector2 v  = _trans.anchoredPosition;
            _trans.anchoredPosition = new Vector2(itemSize.x * i + ox, v.y);
            _trans.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _preferredWidthText);
        }
        
        return isMoveText;
    }
    
    [ContextMenu("Refresh Viewport")]
    void RefreshViewPort()
    {
        // if (willLog) Debug.Log(this + " RefreshViewPort(): " + viewportSize + " --> " + _viewport.rect.size);
        
        var viewRect 		= _viewport.rect;
        viewportSize        = new Vector2(viewRect.width, viewRect.height);
        itemSize            = new Vector2(viewRect.width, viewRect.height);

        if (viewportSize.x <= 1) // 1px too small?
        {
            // if (willLog) Debug.LogWarning(this + " : Invalid viewport! " + viewportSize);
            return;
        }
        
        // if (willLog) Debug.LogWarning(this + " : RefreshViewPort: " + viewportSize);
    }

    // private int frameCounter = 0;
    // private void Update()
    // {
    //     if (!willLog) return;
    //     frameCounter++;
    //     Debug.Log($"Frame: {frameCounter} - view port: {_viewport.rect}");
    // }
    
    public string GetData()
    {
        return _currentText;
    }
    
    public void SetData(string data)
    {
        if (gameObject.activeInHierarchy == false) return;
        
        if (string.IsNullOrEmpty(data)) return;
        _currentText = data;
        
        if (_moveTextRoutine != null) StopCoroutine(_moveTextRoutine);
        _moveTextRoutine = StartCoroutine(MoveText());
    }
    
    private void InstancePrefab()
    {
        _lstItem = new List<ScrollTextItem>();

        var pName = m_Prefab.name;
        m_Prefab.name = $"{pName}_0";
        var item = new ScrollTextItem(m_Prefab, (RectTransform) m_Prefab.transform);
        _lstItem.Add(item);
        
        for (var i = 1; i < nItem; i++)
        {
            var prefab = Instantiate(m_Prefab, _viewport, false);
            prefab.name = $"{pName}_{i}";

            var newItem = new ScrollTextItem(prefab, (RectTransform) prefab.transform);
            
            _lstItem.Add(newItem);
        }
    }
    
	public void OnLocalizeChange()
	{
		SetData(m_Prefab.text);
	}
}
