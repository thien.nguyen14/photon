﻿using System;
using com.team70;
using nano.vs2;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ToggleButtonVS : MonoBehaviour, IPrefabModuleDataLogic<int>, IDataChangeCallback
{
    [ViewOnly] public Button btn;
    [ViewOnly] public VisualState vs;
    
    private Action<object> _callback;
    
    public void Init(PrefabModule module, object initData)
    {
        T70Utility.SetupButton(btn, Toggle);

        if (initData == null) return;
        
        var state = (int) initData;
        vs.state = state;
    }

    public void SetDataChangeCallback(Action<object> callback)
    {
        _callback = callback;
    }

    public void Toggle()
    {
        var index = vs.state > 0 ? 0 : 1;
        vs.state = index;
        _callback?.Invoke(index);
    }

    public int GetData()
    {
        return vs.state;
    }

    public void SetData(int data)
    {
        vs.state = data;
    }
}
