﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.team70;
using UnityEngine;
using UnityEngine.UI;

public class PM_ProgressBarAnchor : MonoBehaviour, IPrefabModuleDataLogic<float>
{
    [Header("Element")] 
    [ViewOnly] public RectTransform m_FillBar;
    
    private Vector2 _anchor;
    private float _minAnchorX;
    private Coroutine _LoadProgressRoutine;
    private bool _inited = false;
    private float _targetX;

    private void OnEnable()
    {
        if (_inited == false) return;
        
        RefreshUI();
    }

    private void OnDisable()
    {
        if(_LoadProgressRoutine != null) StopCoroutine(_LoadProgressRoutine);
    }

    public void Init(PrefabModule module, object initData = null)
    {
        _minAnchorX = module.GetConfig("minAnchorX", 0.09f);
        
        _anchor = new Vector2(_minAnchorX, 1);
        
        m_FillBar.anchorMax = _anchor;
        
        _inited = true;

        if (this.gameObject.activeInHierarchy == false) return;
        
        if(_LoadProgressRoutine != null) StopCoroutine(_LoadProgressRoutine);
        _LoadProgressRoutine = StartCoroutine(RefreshProgressBar());
    }

    

    public float GetData()
    {
        throw new NotImplementedException();
    }

    public void SetData(float data)
    {
        var x = Mathf.Max(_minAnchorX, data);
        x = Mathf.Min(x, 1f);
        _targetX = x;
    }

    private void RefreshUI()
    {
        _anchor.x = _minAnchorX;
        m_FillBar.anchorMax = _anchor;
        
        if(_LoadProgressRoutine != null) StopCoroutine(_LoadProgressRoutine);
        _LoadProgressRoutine = StartCoroutine(RefreshProgressBar());
    }

    private IEnumerator RefreshProgressBar()
    {
        var currentX = _anchor.x;
        _targetX = currentX;
        while (true)
        {
            yield return null;
            
            currentX = Mathf.Lerp(currentX, _targetX, 0.5f);
            
            _anchor.x = currentX;
            m_FillBar.anchorMax = _anchor;
        }
    }
}
