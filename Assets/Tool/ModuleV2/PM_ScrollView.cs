﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.team70;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using CustomScrollRect = ScrollRect2;

[Serializable]
public class ScrollItemData
{
    public PM_ScrollView view;
}

public class PM_ScrollView : MonoBehaviour, IPrefabModuleDataLogic<IList>
{
    [Header("Element")]
    [ViewOnly] public PrefabModule m_view;
    [ViewOnly] public PrefabModule m_Prefab;
    
    [ViewOnly] public RectTransform m_HeaderTrans;
    
    [ViewOnly] public CustomScrollRect m_ScrollRect;
    
    [Header("Config")]
    public bool isVertical;
    public float contentOffset; 
    private bool _reuseItemWhenDisable;
	private string _prefabId;
	
    public int nWrap;
    
    private RectTransform _viewport;
    private RectTransform _content;
    private RecycleView _recycleView;
    
    public IList lstData;
    
    
    public Vector2 itemSize;
    public Vector2 viewportSize;
    public Vector2 contentSize;
	public Vector2 childrenOffsetV;

	private bool _inited;
    private int nRows;
    private int nCols;
    private int nMaxWrapData;

    private void OnEnable()
    {
	    if (_inited == false) return;

	    if (_reuseItemWhenDisable) RefreshRecycleView();
    }

    private void OnDisable()
    {
	    if (_reuseItemWhenDisable) ReturnItemInPool();
    }

    public void Init(PrefabModule module, object initData = null)
    {
		m_view = module;
        _viewport = m_view.transform as RectTransform;
        Assert.IsNotNull(_viewport);
		
        isVertical = module.GetConfig<bool>("isVertical", true);
        contentOffset = module.GetConfig<float>("offset", 0);
        nWrap = module.GetConfig<int>("nWrap", 1);
        _prefabId = module.GetConfig<string>("prefabId", string.Empty);
        _reuseItemWhenDisable = module.GetConfig<bool>("reuseItemWhenDisable", false);

        if (nWrap <= 0) nWrap = 1;

		// Viewport
		var viewRect 		= _viewport.rect;
		viewportSize        = new Vector2(viewRect.width, viewRect.height);

		// Prefab
		m_Prefab = EasyPool.Get<PrefabModule>(_prefabId, transform);
		CalculatePrefabSize();

		// Offset
		CalculateOffset();

		// Content
		_content = CreateContentTransform();

		if (m_HeaderTrans != null) m_HeaderTrans.SetParent(_content, false);
		
		// RecycleView
		EasyPool.Return(m_Prefab.gameObject);
		_recycleView = SetupRecycleView();

		m_ScrollRect = SetupScrollRect();

		_inited = true;
		// Refresh data
		if (initData == null)
		{
			lstData = new List<object>();
			return;
		}
		SetData((IList) initData);
    }

    private void CalculateOffset()
    {
        if (m_HeaderTrans != null)
        {
            var rect = m_HeaderTrans.rect;
            contentOffset += isVertical ? rect.height : rect.width;
        }
		
		var pivot 		= ((RectTransform) m_Prefab.transform).pivot;
		
		if (isVertical)
		{
			var cellCenterX 		= viewportSize.x / nWrap / 2f; // center of the first grid cell
			var cellCenterY			= itemSize.y / 2;
			var itemCenterOffset 	= (pivot.x - 0.5f) * itemSize.x;

			childrenOffsetV 		= new Vector2(cellCenterX + itemCenterOffset, -cellCenterY + (pivot.y - 0.5f) * itemSize.y);
		}
		else 
		{
			var cellCenterY 	= viewportSize.y / nWrap / 2f;
			var itemCenterY		= -itemSize.y * (pivot.y - 0.5f);
			childrenOffsetV 	= new Vector2(pivot.x * itemSize.x, - cellCenterY - itemCenterY);
		}
    }
	
    private void CalculatePrefabSize()
    {
        var itemTrans        = (RectTransform) m_Prefab.transform;
        var itemRect         = itemTrans.rect;
        var ratio      		 = itemRect.height / itemRect.width;
        
        if (isVertical)
        {
            nCols = nWrap;
            itemSize.x    = viewportSize.x / nCols;
            itemSize.y    = itemSize.x * ratio;
            nRows = Mathf.CeilToInt(viewportSize.y / itemSize.y) + 2;
        }
        else
        {
            nRows = nWrap;
            itemSize.y    = viewportSize.y / nRows;
            itemSize.x    = itemSize.y / ratio;
            nCols = Mathf.CeilToInt(viewportSize.x / itemSize.x) + 2;
        }
    }

    private void SetPrefabSize(RectTransform itemTrans)
    {
	    itemTrans.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, itemSize.x);
	    itemTrans.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, itemSize.y);
	    
	    itemTrans.anchorMin = new Vector2(0,1);
	    itemTrans.anchorMax = new Vector2(0,1);
	    itemTrans.sizeDelta = itemSize;
    }
    
    private CustomScrollRect SetupScrollRect()
    {
        var viewGo  = m_view.gameObject;
        viewGo.AddComponent<NonDrawingGraphic>()
            .raycastTarget = true;
            
        var result = viewGo.AddComponent<CustomScrollRect>();
        result.vertical = isVertical;
        result.horizontal = isVertical == false;
        result.viewport = _viewport;
        
        result.content = _content;
        result.onValueChanged.RemoveListener(onChangedValue);
        result.onValueChanged.AddListener(onChangedValue);

        return result;
    }
    
    RectTransform CreateContentTransform()
    {
        var go = new GameObject("Content", typeof(RectTransform));
        go.transform.SetParent(this.transform, false);
        
        var trans = go.transform as RectTransform;
		trans.anchorMin = new Vector2(0, 1);
		trans.anchorMax = new Vector2(0, 1);
		trans.pivot = new Vector2(0, 1);
		trans.sizeDelta = viewportSize;
		
        return trans;
    }

    private void RefreshContentSize()
    {
	    try
	    {
		    if (_content == null) return;

		    var nItemData = 0;
		    if (lstData != null) nItemData = lstData.Count;
	    
		    nMaxWrapData = Mathf.CeilToInt(nItemData / (float)nWrap);
        
		    if (isVertical)
		    {
			    contentSize = new Vector2(viewportSize.x, contentOffset + nMaxWrapData * itemSize.y);
		    }
		    else
		    {
			    contentSize = new Vector2(contentOffset + nMaxWrapData * itemSize.x, viewportSize.y);
		    }

		    // Debug.Log($"{this} ---> RefreshContentSize contentSize: {contentSize} | {itemSize} | {viewportSize} | maxWrap = {nMaxWrapData}");
        
		    _content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, contentSize.x);
		    _content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contentSize.y);
	    }
	    catch (Exception e)
	    {
		    DiscordLogger.DevLog(e.Message);
	    }
	    
    }
    
    RecycleView SetupRecycleView()
    {
        var result = m_view.gameObject.AddComponent<RecycleView>();
        result.gameObject.AddComponent<RectMask2D>();
        
        // setup items
        result.SetupPool(GetItemInPool(nCols * nRows));
        
        return result;
    }
    
    private void onChangedValue(Vector2 percent)
    {
        var wrapPosition = 0f;
        
        if (isVertical)
        {
            var pixels = (1 - percent.y) * (contentSize.y - viewportSize.y) - contentOffset;
            wrapPosition = Mathf.Max(0, pixels / itemSize.y);
        }
        else
        {
            var pixels = percent.x * (contentSize.x - viewportSize.x) - contentOffset;
            wrapPosition = Mathf.Max(0, pixels / itemSize.x);
        }
		
        var itemPosition = ((int)wrapPosition) * nWrap;
        SetItemPositionRecycleView(itemPosition);
    }

    public IList GetData()
    {
        return lstData;
    }

    public void SetData(IList data)
    {
	    if (data == null) return;
	    
        lstData = data;
        RefreshContentSize();

		_recycleView.SetMaxIndex(lstData.Count-1);
		_recycleView.Refresh(true);
		SetItemPositionRecycleView(0);

        _content.anchoredPosition = Vector2.zero;
    }

    private void SetItemPositionRecycleView(float posItem)
    {
	    try
	    {
		    if (lstData == null || _recycleView == null) return;
	    
		    var max = Mathf.Max(0, lstData.Count - nRows * nCols); // -1
		    _recycleView.itemPosition = Mathf.Min(max, posItem);

		    foreach (var item in _recycleView.pool)
		    {
			    if (item == null) continue;

			    var _trans = (RectTransform) item.go.transform;
			    var index = item.Index;
			    var position = index / nWrap;
        
			    var v  = _trans.anchoredPosition;
			    if (isVertical)
			    {
				    var y = -itemSize.y * position - contentOffset;
				    _trans.anchoredPosition = new Vector2(v.x, y + childrenOffsetV.y);
			    }
			    else
			    {
				    var x = itemSize.x * position + contentOffset;
				    _trans.anchoredPosition = new Vector2(x + childrenOffsetV.x, v.y);
			    }
		    }
	    }
	    catch (Exception e)
	    {
		    DiscordLogger.DevLog(e.Message);
	    }
    }

    public void RefreshOffsetSize()
    {
	    contentOffset = m_view.GetConfig<float>("offset", 0);
	    if (m_HeaderTrans != null)
	    {
		    var rect = m_HeaderTrans.rect;
		    contentOffset += isVertical ? rect.height : rect.width;
	    }
	    
	    RefreshContentSize();
	    
	    SetItemPositionRecycleView(_recycleView.itemPosition);
    }

    private List<GameObject> GetItemInPool(int nItem)
    {
	    var data = new ScrollItemData { view = this };
	    var result = new List<GameObject>();
	    for (var i = 0; i < nItem; i++)
	    {
		    var prefab = EasyPool.Get<PrefabModule>(_prefabId, _content);
		    prefab.AttachLogicAndInit(prefab, data);
		    
		    var trans = (RectTransform) prefab.transform;
		    SetPrefabSize(trans);
		    
		    var pos = i % nWrap;
		    var x = isVertical ? itemSize.x * pos : 0;
		    var y = isVertical ? 0 : (-itemSize.y * pos);
		    trans.anchoredPosition = new Vector2(x, y) + childrenOffsetV;
		    
		    result.Add(prefab.gameObject);
	    }

	    return result;
    }
    
    private void RefreshRecycleView()
    {
	    _recycleView.SetupPool(GetItemInPool(nCols * nRows));
	    
	    SetData(lstData);
    }

    private void ReturnItemInPool()
    {
	    var pool = _recycleView.pool;
	    foreach (var item in pool)
	    {
		    EasyPool.Return(item.go);
	    }
    }
}
