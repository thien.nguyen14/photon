﻿using com.team70;
using UnityEngine;
using Async = com.team70.Async;

public class PM_VerticalLayout : MonoBehaviour, IPrefabModuleDataLogic<RectTransform[]>
{
    [Header("Config")] 
    [ViewOnly] public float top;
    [ViewOnly] public float space;
    [ViewOnly] public float bot;
    
    [Header("Element")] 
    [ViewOnly] public RectTransform[] m_ListItem;
    
    private RectTransform _content;
    
    public void Init(PrefabModule module, object initData = null)
    {
        if (initData != null) m_ListItem = (RectTransform[]) initData;

        top = module.GetConfig("top", 0f);
        space = module.GetConfig("space", 0f);
        bot = module.GetConfig("bot", 0f);

        SetupContent();

        SetupItems();
        
        Async.Call(RefreshSize);
    }

    

    public RectTransform[] GetData()
    {
        return m_ListItem;
    }

    public void SetData(RectTransform[] data)
    {
        if (data == null) return;

        m_ListItem = data;
        
        SetupItems();
        
        Async.Call(RefreshSize);
    }
    
    private void SetupContent()
    {
        var pos = new Vector2(0, 1);
        _content = (RectTransform) this.transform.parent;
        var rect = _content.rect;
        _content.anchorMin = pos;
        _content.anchorMax = pos;
        _content.pivot = pos;
        _content.sizeDelta = new Vector2(rect.width, rect.height);

        Async.Call(() => _content.anchoredPosition = Vector2.zero);
    }

    private void SetupItems()
    {
        if(m_ListItem == null) return;

        foreach (var item in m_ListItem)
        {
            item.anchorMin = new Vector2(0, 1);
            item.anchorMax = new Vector2(1, 1);
        }
    }
    
    public void RefreshSize()
    {
        if (m_ListItem == null) return;

        var height = top;

        foreach (var item in m_ListItem)
        {
            if (item.gameObject.activeSelf == false) continue;

            var itemHeight = item.rect.height;
            var pos = item.anchoredPosition;
            
            pos.y = -(height + itemHeight * (1 - item.pivot.y));
            item.anchoredPosition = pos;
            
            height += itemHeight + space;
        }

        if (Mathf.Abs(height - top) > Mathf.Epsilon) height += bot - space;

        if (Mathf.Abs(_content.rect.height - height) <= Mathf.Epsilon) return;

        _content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
    }
}
