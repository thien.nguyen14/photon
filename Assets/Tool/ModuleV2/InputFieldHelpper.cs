﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputFieldHelpper : MonoBehaviour, ISelectHandler, IPointerClickHandler, IDeselectHandler
{
    [Header("Element")] 
    [ViewOnly] public InputField m_Input;
    
    private bool _inited;
    private Action<bool> _onFocusChange;
    
    public void Init(InputField inputField = null, Action<bool> callback = null)
    {
        if (_inited) return;

        m_Input = inputField;
        _onFocusChange = callback;
        
        _inited = true;
    }

    public void OnSelect(BaseEventData eventData)
    {
        _onFocusChange?.Invoke(true);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Left) return;
        
        _onFocusChange?.Invoke(true);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        _onFocusChange?.Invoke(false);
    }
}