﻿using System;
using System.Collections;
using com.team70;
using UnityEngine;
using UnityEngine.UI;

public class PM_CircleCountDown : MonoBehaviour, IPrefabModuleLogic, IDataChangeCallback
{
    [Header("Element")] 
    [ViewOnly] public Image m_FillAmount;
    [ViewOnly] public Text m_TimeTxt;
    
    public float currTimeCountDown;
    public float totalTimeCountDown;
    
    private Coroutine _countDownRoutine;
    private Action<object> _callBackFinishCountDown;

    public void Init(PrefabModule module, object initData = null)
    {
        throw new NotImplementedException();
    }

    public void SetDataChangeCallback(Action<object> callback)
    {
        _callBackFinishCountDown = callback;
    }
    
    public void StartCountDown(float duration)
    {
        totalTimeCountDown = duration;
        currTimeCountDown = duration;
        m_FillAmount.fillAmount = 1f;
        m_TimeTxt.text = $"{Mathf.RoundToInt(duration)}";

        if (_countDownRoutine != null) StopCoroutine(_countDownRoutine);
        _countDownRoutine = StartCoroutine(CountDown());
    }

    public void PauseCountDown()
    {
        if (_countDownRoutine != null) StopCoroutine(_countDownRoutine);
    }
    
    public void ContinueCountDown()
    {
        if (_countDownRoutine != null) StopCoroutine(_countDownRoutine);
        _countDownRoutine = StartCoroutine(CountDown());
    }
    
    private IEnumerator CountDown()
    {
        var waitEndOfFrame = new WaitForEndOfFrame();
        
        while (currTimeCountDown > 0)
        {
            currTimeCountDown -= Time.deltaTime;
            m_FillAmount.fillAmount = currTimeCountDown / totalTimeCountDown;
            m_TimeTxt.text = $"{Mathf.RoundToInt(currTimeCountDown)}";
            yield return waitEndOfFrame;
        }
        
        m_FillAmount.fillAmount = 0;
        m_TimeTxt.text = "0";
        HandleFinishCountDown();
    }
    
    public void OnClickSpeedUp()
    {
        if (currTimeCountDown <= 0) return;

        currTimeCountDown = Mathf.Max(0, Mathf.RoundToInt(currTimeCountDown) - 1f);
    }

    private void HandleFinishCountDown()
    {
        _callBackFinishCountDown?.Invoke(null);   
    }
}
