﻿using System;
using com.team70;
using nano.vs2;
using UnityEngine;
using UnityEngine.UI;

public class StateButtonVS : MonoBehaviour, IPrefabModuleDataLogic<int>, IDataChangeCallback
{
    [ViewOnly] public Button[] arrBtn;
    [ViewOnly] public VisualState vs;
    
    private Action<object> _callback;
    
    public void Init(PrefabModule module, object initData = null)
    {
        if (arrBtn.Length != vs.nStates)
        {
            Debug.LogWarning("Length button different n state");
            return;
        }

        for (var i = 0; i < arrBtn.Length; i++)
        {
            var index = i;
            T70Utility.SetupButton(arrBtn[i], () => HandleButtonClick(index));
        }

        if (initData == null) return;
        
        var state = (int) initData;
        vs.state = state;
    }

    public void SetDataChangeCallback(Action<object> callback)
    {
        _callback = callback;
    }

    private void HandleButtonClick(int index)
    {
        vs.state = index;
        _callback?.Invoke(index);
    }

    public int GetData()
    {
        throw new NotImplementedException();
    }

    public void SetData(int data)
    {
        vs.state = data;
    }
}
