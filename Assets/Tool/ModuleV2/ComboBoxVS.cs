﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.team70;
using nano.vs2;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class ComboBoxItemData
{
    public ComboBoxVS parent;
    public bool isActiveItem;
    public int index;
}

[Serializable]
public class ComboBoxData
{
    public int currentIdx;
    public IList lstData;
}

public class ComboBoxVS : MonoBehaviour, IPrefabModuleLogic, IDataChangeCallback
{
    [Header("Elements")]
    [ViewOnly] public VisualState m_OpenCloseVS;
    
    [ViewOnly] public RectTransform m_Content;
    
    [ViewOnly] public PrefabModule m_Prefab;
    [ViewOnly] public PrefabModule m_SelectItemPM;

    public IList lstData;

    private List<PrefabModule> _lstItem;
    private int _selectedIndex;
    private Action<object> _callback;

    private void OnEnable()
    {
        if (lstData == null || lstData.Count < 2) return;

        m_OpenCloseVS.state = 0;
    }
    
    public void Init(PrefabModule module, IList lstData, int selectedIndex = 0)
    {
        this.lstData = lstData;
        _selectedIndex = selectedIndex;
        _lstItem = new List<PrefabModule>();
        
        var currentData = new ComboBoxItemData {index = _selectedIndex, parent = this, isActiveItem = true};
        
        m_SelectItemPM.script.autoInit = false;
        m_SelectItemPM.AttachLogicAndInit(m_SelectItemPM, currentData);
        
        if (lstData.Count < 2)
        {
            m_OpenCloseVS.state = 2;
            return;
        }
        
        m_Prefab.script.autoInit = false;
        _lstItem.Add(m_Prefab);
        
        // GENERATE COMBO BOX ITEMS
        for (var i = 1; i < this.lstData.Count; i++)
        {
            var data = new ComboBoxItemData {index = i, parent = this, isActiveItem = false};
            var item = Instantiate(m_Prefab, m_Content);
            item.AttachLogicAndInit(item, data);
            
            _lstItem.Add(item);
        }
        
        var data0 = new ComboBoxItemData {index = 0, parent = this, isActiveItem = false};
        m_Prefab.AttachLogicAndInit(m_Prefab, data0);
    }
    
    public void Init(PrefabModule module, object initData)
    {
        if (initData == null) return;
        var data = (ComboBoxData) initData;
        Init(module, data.lstData, data.currentIdx);
    }

    public int GetSelectedIndex()
    {
        return _selectedIndex;
    }
    
    public void SetSelectedIndex(object data)
    {
        m_OpenCloseVS.state = 0;
        
        var index = (int) data;
        if (index == _selectedIndex) return;
        _selectedIndex = index;

        m_SelectItemPM.SetLogicData(_selectedIndex);
        _callback?.Invoke(data);
    }
    
    public void ToggleOpenCloseComboxBox()
    {
        m_OpenCloseVS.state = (m_OpenCloseVS.state == 0) ? 1 : 0;

        if (m_OpenCloseVS.state == 0) return;

        foreach (var item in _lstItem)
        {
            item.SetLogicData(_selectedIndex);
        }
    }
    
    public void SetDataChangeCallback(Action<object> callback)
    {
        _callback = callback;
    }
}
