﻿using System;
using System.Collections.Generic;
using System.Linq;
using T70.VariableAsset;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Async = com.team70.Async;
using Random = UnityEngine.Random;

public class T70Utility
{
    public static void SetupButton(Button btn, UnityAction action, bool addEffect = true)
    {
        if (btn == null || action == null) return;
        
        btn.onClick.RemoveListener(action);
        btn.onClick.AddListener(action);

        if (addEffect == false) return;

        AddButtonEffect(btn);
    }

    public static void AddButtonEffect(Button btn)
    {
        var effect = btn.gameObject.AddComponent<InteractionEffect>();
        if (effect == null)
        {
            effect = btn.gameObject.AddComponent<InteractionEffect>();
        }

        effect.targetScale = new Vector3(0.9f, 0.9f, 0.9f);
        effect.btn = btn;
    }
    
    public static void SetupButtons(params object[] buttonActionList)
    {
        for (var i = 0; i < buttonActionList.Length; i += 2)
        {
            var btn = buttonActionList[i] as Button;
            if (btn == null) continue;

            var action = buttonActionList[i + 1] as UnityAction;
            if (action == null) continue;
            
            btn.onClick.RemoveListener(action);
            btn.onClick.AddListener(action);
            
            AddButtonEffect(btn);
        }
    }

    public static BackButtonHelper AddBackButtonHelper(GameObject go, UI_Type type, UnityAction callBack)
    {
        var result = go.gameObject.AddComponent<BackButtonHelper>();
        result.Init(type, callBack);

        return result;
    }
}
