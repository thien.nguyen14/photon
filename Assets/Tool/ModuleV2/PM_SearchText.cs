﻿using System;
using com.team70;
using nano.vs2;
using T70.VariableAsset;
using UnityEngine;
using UnityEngine.UI;

public class PM_SearchText : MonoBehaviour, IPrefabModuleDataLogic<string>, IDataChangeCallback
{
    [Header("Element")] 
    [ViewOnly] public InputField m_Input;
    [ViewOnly] public InputFieldHelpper m_InputFieldHelpper;
    
    [ViewOnly] public Animator m_InputAnimator;
    
    [ViewOnly] public Button m_ClearBtn;
    [ViewOnly] public Button m_CancelBtn;
    
    [ViewOnly] public VisualState m_ClearIconVS;

    private string _resultSearch;
    private bool _inited;
    private Action<object> _callBack;
    private int _showHashId;
    private int _hideHashId;
    private bool _isFocus;
    
    public void Init(PrefabModule module, object initData = null)
    {
        if (_inited) return;
        _inited = true;

        T70Utility.SetupButton(m_ClearBtn, OnClickClearBtn);
        T70Utility.SetupButton(m_CancelBtn, OnClickCancelBtn);

        m_InputFieldHelpper = m_Input.gameObject.AddComponent<InputFieldHelpper>();
        m_InputFieldHelpper.Init(m_Input, OnInputFieldFocusChange);

        SetFocus(false);
        m_Input.onEndEdit.AddListener(HandleOnEndEditInputField);
        m_Input.onValueChanged.AddListener(HandleOnValueChangedInputField);

        _showHashId = Animator.StringToHash("InputSearchTyping");
        _hideHashId = Animator.StringToHash("InputSearchDefault");
    }

    private void OnInputFieldFocusChange(bool isFocus)
    {
        if (isFocus == false) return;
        
        SetFocus(true);
    }

    private void SetFocus(bool isFocus)
    {
        if (gameObject.activeInHierarchy == false) return;

        m_InputAnimator.Play(isFocus ? _showHashId : _hideHashId);

        _isFocus = isFocus;
    }
    
    public void SetDataChangeCallback(Action<object> callback)
    {
        _callBack = callback;
    }

    public string GetData()
    {
        return _resultSearch;
    }

    public void SetData(string data)
    {
        if (string.IsNullOrEmpty(data) == false)
        {
            m_Input.text = data;
            return;
        }
        
        m_Input.text = string.Empty;
        SetFocus(false);
    }

    private void HandleOnEndEditInputField(string resultSearch)
    {
        HandleInputSearch(resultSearch);
    }

    private void HandleOnValueChangedInputField(string resultSearch)
    {
        HandleInputSearch(resultSearch);
    }
    
    private void HandleInputSearch(string resultSearch)
    {
        m_ClearIconVS.state = string.IsNullOrEmpty(resultSearch) ? 0 : 1;
        _resultSearch = resultSearch;
        Async.Call(() => _callBack?.Invoke(_resultSearch), 0.02f, "SetDataSearch");
    }
    
    private void OnClickClearBtn()
    {
        Async.Kill("SetDataSearch");
        m_Input.text = string.Empty;
        SetFocus(true);
    }

    private void OnClickCancelBtn()
    {
        Async.Kill("SetDataSearch");
        m_Input.text = string.Empty;
        SetFocus(false);
    }
}
