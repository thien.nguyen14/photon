﻿using System;
using com.team70;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class RippleData
{
    public float time;
    public Vector3 targetScale;
    public float timeDelay;
}

public class PM_RippleButton : MonoBehaviour, IPrefabModuleLogic, IDataChangeCallback
{
    const string TWEEN_ID = "TWEEN_ID";
    
    [Header("Element")]
    [ViewOnly] public Button m_Btn;
    [ViewOnly] public Image[] m_ArrImageEffect;

    [Header("Config")] 
    [ViewOnly] public bool autoPlay;
    
    private Action<object> _callback;
    private RippleData _data;
    
    private void OnEnable()
    {
        if (m_ArrImageEffect == null && autoPlay == false) return;

        Play();
    }
    
    private void OnDisable()
    {
        DOTween.Kill(GetInstanceID() + TWEEN_ID);
    }
    
    public void Init(PrefabModule module, object initData = null)
    {
        T70Utility.SetupButton(m_Btn, OnClickBtn);

        autoPlay = module.GetConfig("autoPlay", true);
        var time = module.GetConfig("time", 1f);
        var timeDelay = module.GetConfig("timeDelay", 1f);
        var x = module.GetConfig("scaleX", 1f);
        var y = module.GetConfig("scaleY", 1f);
        
        _data = new RippleData() {time = time, timeDelay = timeDelay, targetScale = new Vector3(x, y, 1f)};
        Play();
    }

    public void SetDataChangeCallback(Action<object> callback)
    {
        _callback = callback;
    }

    private void OnClickBtn()
    {
        _callback?.Invoke(null);
    }
    
    private void Play()
    {
        DOTween.Kill(GetInstanceID() + TWEEN_ID);
        
        Vector3 origin = Vector3.one;
        //Vector3 targetScale = new Vector3(1.2f,1.5f,1);
        for(int i =0; i < m_ArrImageEffect.Length; i++)
        {
            var item = m_ArrImageEffect[i];
            Color col = item.color;
            DOTween.To(() => 0f, x =>
            {
                item.transform.localScale = Vector3.Lerp(origin, _data.targetScale, x);
                col.a = Mathf.Lerp(.5f, 0, x);
                item.color = col;
            }, 1f, _data.time).SetDelay(_data.timeDelay * i).SetLoops(-1).SetId(GetInstanceID()+ TWEEN_ID);
        }
    }

    public void StartEffect()
    {
        foreach (var img in m_ArrImageEffect)
        {
            img.enabled = true;
        } 
        
        Play();
    }

    public void StopEffect()
    {
        DOTween.Kill(GetInstanceID() + TWEEN_ID);
        
        foreach (var img in m_ArrImageEffect)
        {
            img.enabled = false;
        }   
    }
}
