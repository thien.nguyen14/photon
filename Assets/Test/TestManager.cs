using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

#if !UNITY_EDITOR && UNITY_WEBGL
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace IngameDebugConsole
{
    public static class Web3s
    {
        [DllImport( "__Internal" )]
        public static extern void getCookie( string myUrl );
    }
}
#endif

public class TestManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(LogExample(UnityWebRequest.kHttpVerbPOST));
        
        //StartCoroutine(LogExample(UnityWebRequest.kHttpVerbHEAD));
        
#if !UNITY_EDITOR && UNITY_WEBGL
        var myUrl = "https://fbmarket.klabs.life/";
        Debug.LogError($"Test");
        IngameDebugConsole.Web3s.getCookie(myUrl);
        //Debug.LogError($"getCookie >>> Cookie: {IngameDebugConsole.Web3s.getCookie(myUrl)}");
#endif
    }
    
    private IEnumerator LogExample(string method)
    {
        var www = new UnityWebRequest {method = method, url = "https://fbmarket.klabs.life"};
        www.GetRequestHeader("Cookie");
        www.downloadHandler = new DownloadHandlerBuffer();
        
        var operation = www.SendWebRequest();
        while (!operation.isDone)
        {
            yield return null;
        }
            
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError($"Get Cookie fail >>> Method: {method} || result: {www.result} || error: {www.error}");
        }

        var response = www.downloadHandler.text;
        
        yield return new WaitForSeconds(1f);
        
        Debug.LogError($"Method: {method} || Cookie: {response}");
    }
}
