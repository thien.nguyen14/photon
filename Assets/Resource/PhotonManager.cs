using System;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

public partial class PhotonManager
{
    private static bool _isDestroyed;
    private static bool _isPlaying;
    
    private static PhotonManager _api;
    internal static PhotonManager Api
    {
        get
        {
            if (_isDestroyed) return null;
            if (_isPlaying) return _api;
            return Application.isPlaying ? CreateInstance() : null;
        }
    }
    
    public static PhotonManager CreateInstance()
    {
        if (_isDestroyed) return null;
        return _api ? _api : _api = new GameObject("~PhotonManager").AddComponent<PhotonManager>();
    }

    public static void SetConnectedSceneName(string sceneName)
    {
        Api.connectedSceneName = sceneName;
    }
    
    public static void AddPlayerEnteredRoomCallBack(Action callBack)
    {
        Api.handlePlayerEnteredRoom -= callBack;
        Api.handlePlayerEnteredRoom += callBack;
    }

    public static void AddPlayerLeftRoomCallBack(Action callBack)
    {
        Api.handlePlayerLeftRoom -= callBack;
        Api.handlePlayerLeftRoom += callBack;
    }
    
    public static void AddLeftRoomCallBack(Action callBack)
    {
        Api.handleLeftRoom -= callBack;
        Api.handleLeftRoom += callBack;
    }
    
    public static void StartConnect()
    {
        Api.Connect();
    }
    
    public static void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public static GameObject InstanceGO(string objectName)
    {
        return PhotonNetwork.Instantiate(objectName, new Vector3(0f,5f,0f), Quaternion.identity, 0);
    }
}

public partial class PhotonManager : MonoBehaviourPunCallbacks, IPunObservable
{
    [SerializeField] private string connectedSceneName = string.Empty;
    
    [SerializeField] private byte maxPlayersPerRoom = 2;

    [SerializeField] private Action handlePlayerEnteredRoom;

    [SerializeField] private Action handlePlayerLeftRoom;

    [SerializeField] private Action handleLeftRoom;
    
    [SerializeField] private Action handleProcessInputs;
    
    private bool isConnecting;
    
    private string gameVersion = "1";
    
    private void Awake()
    {
        if (_api != null && _api != this)
        {
            Debug.LogWarning("Multiple PhotonManager found!!!");
            Destroy(this);
            return;
        }
			
        _api = this;
        _isDestroyed = false;
        _isPlaying = true;
        DontDestroyOnLoad(_api);
        
        // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.AutomaticallySyncScene = true;
    }
    
    private void Update()
    {
        // we only process Inputs if we are the local player
        if (photonView.IsMine == false) return;
        
        handleProcessInputs?.Invoke();
    }
    
    public void Connect()
    {
        // keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
        isConnecting = true;

        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.IsConnected)
        {
            // We need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnJoinRandomFailed() and we'll create one.
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {

            // We must first and foremost connect to Photon Online Server.
            PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = this.gameVersion;
        }
    }
}

// MonoBehaviourPunCallbacks CallBacks
public partial class PhotonManager
{
    // Called after the connection to the master is established and authenticated
    public override void OnConnectedToMaster()
    {
        if (isConnecting)
        {
            // The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnJoinRandomFailed()
            PhotonNetwork.JoinRandomRoom();
        }
    }
    
    // Called when a JoinRandom() call failed. Most likely all rooms are full or no rooms are available.
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        // We failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = this.maxPlayersPerRoom});
    }
    
    // Called after disconnecting from the Photon server.
    public override void OnDisconnected(DisconnectCause cause)
    {
        isConnecting = false;
    }
    
    // Called when entering a room (by creating or joining it). Called on all clients
    public override void OnJoinedRoom()
    {
        // We only load if we are the first player, else we rely on  PhotonNetwork.AutomaticallySyncScene to sync our instance scene.
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            // Load the Room Level. 
            PhotonNetwork.LoadLevel(connectedSceneName);
        }
    }
    
    // Called when a Photon Player got connected.
    public override void OnPlayerEnteredRoom(Player other)
    {
        if (PhotonNetwork.IsMasterClient == false) return;
        
        handlePlayerEnteredRoom?.Invoke();
    }
    
    // Called when a Photon Player got disconnected
    public override void OnPlayerLeftRoom(Player other)
    {
        if (PhotonNetwork.IsMasterClient == false) return;
        
        handlePlayerLeftRoom?.Invoke();
    }
    
    // Called when the Local player left the room
    public override void OnLeftRoom()
    {
        handleLeftRoom?.Invoke();
    }
    
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
        }
        else
        {
            // Network player, receive data
        }
    }
}